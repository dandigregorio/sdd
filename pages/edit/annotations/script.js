$(document).ready(function () {

    var data = []
    var myLabels = []

    data = loadData()


    var annotations = [];
    var saveBg, saveCol;
    var last_ann = 0;
    var g = new Dygraph(document.getElementById("div_g4"),
            //NoisyData,
            data,
            {
                labels: myLabels,
                errorBars: false, // DEVIAZIONE STANDARD
                showRangeSelector: true,
                drawGrid: false,
                drawCallback: function (g) {
                    var ann = g.annotations();
                },
                pointClickCallback: function (event, p) {
                    // Check if the point is already annotated.
                    if (p.name.substr(0, 1) == "_")
                        return;
                    if (p.annotation)
                        return;
                    // If not, add one.
                    var ann = {
                        series: p.name,
                        xval: p.xval,
                        shortText: "X",
                        text: "Clicca per rimuovere",
                        cssClass: 'myAnnotation',
                        tickHeight: -7
                    };
                    var anns = g.annotations();
                    anns.push(ann);
                    g.setAnnotations(anns);
                },
                highlightCallback: function (e, x, pts, row) {},
                unhighlightCallback: function (e) {},
                annotationMouseOverHandler: function (ann, point, dg, event) {
                    saveBg = ann.div.style.backgroundColor;
                    saveCol = ann.div.style.color;
                    ann.div.style.backgroundColor = ann.div.style.color;
                    ann.div.style.color = "white";
                },
                annotationMouseOutHandler: function (ann, point, dg, event) {
                    ann.div.style.backgroundColor = saveBg;
                    ann.div.style.color = saveCol;
                },
                annotationClickHandler: function (ann, point, dg, event) {
                    var anns = g.annotations();
                    removeAnn(anns, ann);
                },
                legendFormatter: function (data) {
                    data.series = [data.series[0], data.series[1]];  // pick whichever series you want to keep
                    return Dygraph.Plugins.Legend.defaultFormatter.call(this, data);
                },
                drawPoints: true,
                pointSize: 2,
                //per-series options http://dygraphs.com/gallery/#g/per-series   
                series: {

                    _A: {
                        drawPoints: false,
                        strokeWidth: 0.1,
                        highlightCircleSize: 0
                    },
                    _B: {
                        drawPoints: false,
                        strokeWidth: 0.1,
                        highlightCircleSize: 0,
                        legend: false
                    }
                },
                //per-axis options http://dygraphs.com/per-axis.html  
                axes: {
                    x: {
                        axisLabelFormatter: function (x) {
                            return x;
                        }
                    },
                    y: {
                        axisLabelFormatter: function (y) {
                            return y;
                        }
                    }
                }
            });
    function removeAnn(MainAnns, DelAnn) {
        for (var k in MainAnns) {
            if (MainAnns.hasOwnProperty(k)) {
                var anns = g.annotations();
                if (MainAnns[k].series + MainAnns[k].xval == DelAnn.series + DelAnn.xval) {
                    anns.splice(k, 1)
                    g.setAnnotations(anns);
                    break;
                }
            }
        }

    }

// ######################## LISTENERS ################################# 
    $('li.cmdRestore').click(function () {
        data = []
        data = loadData()
        g.updateOptions({'file': data}, false);
        g.setAnnotations([])
    });
    $('li.cmdInvalida').click(function () {
        var anns = g.annotations();
        var series = g.getLabels()
        console.log("Annotations", anns)
        console.log("Series", series)
        // per ogni punto marcato
        for (var k in anns) {
// trova il num colonna nell'array delle labels (Y)
            if (anns.hasOwnProperty(k)) {
                var y = g.getLabels().indexOf(anns[k]['series'])
                console.log("Numero colonna di " + anns[k]['series'] + " = " + y)
                for (var x in data) {
                    if (data[x][0] == anns[k]['xval']) {
                        console.log("Numero colonna di " + anns[k]['xval'] + "=" + x)
                        data[x][y] = null
                        g.updateOptions({'file': data}, false);
                        g.setAnnotations([])
                        break;
                    }
                }
            }
        }


    });
// ###################### FUNZIONI ##################################

    function loadData() {
        var myData = [];
        myLabels = ["Data", "A", "B", "_A", "_B"]
        myData.push(
                [1202966640, 62, 39, 62, 39],
                [1203053040, 59, 44, 59, 44],
                [1203139440, 62, 42, 62, 42],
                [1203225840, 57, 45, 57, 45],
                [1203312240, 54, 44, 54, 44],
                [1203398640, 55, 36, 55, 36])
        return myData;
    }




});


