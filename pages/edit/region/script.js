$(document).ready(function () {
    var Rx = null;
    var Ry = null;
    var Rw = null;
    var Rh = null;
    var canvasx;
    var canvasy;
    var d = $("div.col-md-4");
    var rDiv = document.getElementById('region');
    var cvsBackup;
    var dragging = false; // trigger per eseguire le funzioni legate al movimento del mouse
    var gCanvas = null; // canvas del grafico
    var graphPos; // Posizione X/Y del grafico in pagina
    var ctx;
    var sel = [];
    sel['x'] = []; //X1 e X1
    sel['y'] = [];


    var processed = [];

    function mouseMoving(event, g, context) {

        var RANGE = 3;

        if (dragging) {
            graphPos = Dygraph.findPos(g.graphDiv);

            canvasx = Dygraph.pageX(event) - graphPos.x;
            canvasy = Dygraph.pageY(event) - graphPos.y;

            if (Rx == null && Ry == null) {
                Rx = canvasx;
                Ry = canvasy;
            }
            g.updateOptions({'file': "/temperatures.csv"}, false);
            d.html("")
            d.append("Angolo grafico = " + graphPos.x + ":" + graphPos.y + "<br>")
            d.append("Angolo regione = " + Rx + ":" + Ry + "<br>")
            Rw = canvasx - Rx;
            Rh = canvasy - Ry;
            d.append("Lati regione: " + Rw + "px/" + Rh + "px<br>")

            drawRegion();
        }

    }

    function drawRegion() {

        if (Rw > 0 && Rh > 0) { // se drag = GiuSx
            rDiv.style.left = graphPos.x + Rx + 'px';
            rDiv.style.top = graphPos.y + Ry + 'px';
            rDiv.style.width = Rw - 3;
            rDiv.style.height = Rh - 3;

            sel['x'][0] = Rx;
            sel['y'][0] = Ry;
            sel['x'][1] = Rx + Rw - 3;
            sel['y'][1] = Ry + Rh - 3;
        } else if (Rw < 0 && Rh > 0) { // se drag = GiuDx
            rDiv.style.left = graphPos.x + canvasx + 'px';
            rDiv.style.top = graphPos.y + Ry + 'px';
            rDiv.style.width = Math.abs(Rw) - 3;
            rDiv.style.height = Rh - 3;

            sel['x'][0] = canvasx;
            sel['y'][0] = Ry;
            sel['x'][1] = canvasx + Math.abs(Rw) - 3;
            sel['y'][1] = Ry + Rh - 3;
        } else if (Rw < 0 && Rh < 0) { // se drag = SuDx
            rDiv.style.left = graphPos.x + canvasx + 'px';
            rDiv.style.top = graphPos.y + canvasy + 'px';
            rDiv.style.width = Math.abs(Rw) - 3;
            rDiv.style.height = Math.abs(Rh) - 3;

            sel['x'][0] = canvasx;
            sel['y'][0] = canvasy;
            sel['x'][1] = canvasx + Math.abs(Rw) - 3;
            sel['y'][1] = canvasy + Math.abs(Rh) - 3;
        } else if (Rw > 0 && Rh < 0) { // se drag = SuDx
            rDiv.style.left = graphPos.x + Rx + 'px';
            rDiv.style.top = graphPos.y + canvasy + 'px';
            rDiv.style.width = Rw - 3;
            rDiv.style.height = Math.abs(Rh) - 3;

            sel['x'][0] = Rx;
            sel['y'][0] = canvasy;
            sel['x'][1] = Rx + Rw - 3;
            sel['y'][1] = canvasy + Math.abs(Rh) - 3;
        }
    }

    //CLICK  DEL MOUSE
    function mouseDown(event, g, context) {
        context.initializeMouseDown(event, g, context);
        dragging = true;
        ctx = gCanvas;
        mouseMoving(event, g, context); // in case the mouse went down on a data point.
        console.log("Clicking...")
    }

    // RILASCIO CLICK DEL MOUSE
    function mouseUp(event, g, context) {
        resetRegion();
        d.html("")
        ctx.fillStyle = "#FFFF00";
        ctx.globalAlpha = 0.2;
        ctx.fillRect(sel['x'][0], sel['y'][0], sel['x'][1] - sel['x'][0], sel['y'][1] - sel['y'][0]);
        if (dragging) {
            dragging = false;
        }
        console.log("Released, launch point selecion")
        findPointsInside();
    }


    function findPointsInside() {
        console.log("Scorro myData per validare/invalidare i punti nella selezione")
        //per ogni (date) in  myData[x]
            //per ogni (idVal) di (date)
        
                //pointCoord= g[x].toDomCoords(date, idVal)
                // if ( (sel['x'][0] < pointCoord[0] < sel['x'][1]) && (sel['y'][0] < && pointCoord[1] < sel['y'][1] )
                    // toggle myData[n][(date)][(idVal)]          
        
            //per ogni (val) di (date)
        // fine per ogni riga di myData[x]
    }

    function resetRegion() {
        Rx = null;
        Ry = null;
        rDiv.style.left = '0px';
        rDiv.style.top = '0px';
        rDiv.style.width = 0
        rDiv.style.height = 0
    }


    // DOPPIO CLICK DEL MOUSE
    function dblClick(event, g, context) {
        restorePositioning(g);
    }

    // EVIDENZIA IL PUNTO
    function drawMarker(x, y) {
        ctx.strokeStyle = "#000000";
        ctx.fillStyle = "#FFFF00";
        ctx.beginPath();
        ctx.arc(x, y, 5, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.stroke();
        ctx.fill();
    }

    //PIAZZA IL CANVAS DEL GRAFICO
    function captureCanvas(canvas, area, g) {
        gCanvas = canvas;

    }

    // PROCEDURA RICHIAMATA DAL DOPPIO CLICK        
    function restorePositioning(g) {
        processed = [];
        stored = [];
        g.updateOptions({
            dateWindow: null,
            valueRange: null
        });
    }

    //GENERA IL GRAFICO
    var g = new Dygraph(document.getElementById("div_g4"), "/temperatures.csv", {
        errorBars: false, // DEVIAZIONE STANDARD
        drawPoints: true,
        showRangeSelector: true,
        interactionModel: {
            'mousedown': mouseDown,
            'mousemove': mouseMoving,
            'mouseup': mouseUp,
            'dblclick': dblClick
        },
        underlayCallback: captureCanvas
    });


});
