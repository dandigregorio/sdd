<!--Datatables for bootstrap-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js"></script>

<script src="/js/plugin/dygraph/dygraph.js"></script>
<script src="/js/plugin/dygraph/extras/synchronizer.js"></script>
<link rel="stylesheet" src="/js/plugin/dygraph/dygraph.css" />