<?php
/*
------------------
Language: English
------------------
*/
 
// META PAGE, DO NOT CHANGE CONSTANTS NAMES
define("META_TITLE", "Gluten free advisor");
define("META_DESCRIPTION", "Gastromama let you uncover locations where you can eat gluten-free food, also those not included in special lists");
// END META PAGE

// CUSTOM PAGE TEXT HERE BELOW

$lang = array();
 
$lang['SITE_NAME'] = 'Gastromama';
$lang['PAGE_TITLE'] = 'Welcome to Gastromama';

$lang['HEADER_TITLE'] = 'Gastromama, the gluten free advisor';
$lang['SLOGAN'] = 'We help <a href="#users" style="font-weight:600">people</a> to eat gluten-free everywhere, and <a href = "#business" style = "font-weight: 600" >companies</a> to optimize the strategies in the gluten-free industry';

$lang['HEADING_TITLE'] = "We help people to eat gluten-free everywhere";
$lang['HEADING'] = "Gluten free  Advisor by Gastromama is an app that analyzes on-line conversations and public data to discover the places attended by those who eat gluten like you, and locates these place on your phone in seconds.";

define("SEARCH_TITLE", "Gluten free locations discovered recently");

define("CALL_TITLE_1","Different");
define("CALL_TEXT_1","We do not display addresses, but we search the web, just as you, finding locations visited by those who eat gluten-free, wherever they may be.");

define("CALL_TITLE_2","Automagic");
define("CALL_TEXT_2","It works everywhere, in your city as well as the other side of the world. Install it on your mobile to discover in seconds the location of the places and other useful information such as telephone and website. You can trace the route with the GPS of your phone.");

define("CALL_TITLE_3","Quick");
define("CALL_TEXT_3","It takes just a few moments to find out where to eat gluten-free");

define("TRY_NOW","Try it now. Find out where to eat gluten free...");

/* BUSINESS */

$lang['HEADING_TITLE2'] = "We help companies to optimize the strategies in the gluten-free industry";
$lang['HEADING2'] = "Our technology turns public data into relevant information for those companies that offer products and training in the gluten free market.";

define("CALL_TITLE_12",">53.000");
define("CALL_TEXT_12","Places offering gluten free options unvealed worldwide until now");

define("CALL_TITLE_22","80%");
define("CALL_TEXT_22","Restaurants in Rome, Milan and Neaples that offers gluten free options withour any kind of certification about gluten cross-contamination, and that might need of training or assistance to improve the skill of the staff or the quality of the gluten free offer");

define("CALL_TITLE_32","5%");
define("CALL_TEXT_32","The restaurants offering gluten free options to users in Italy.");



define("CALL_TITLE_13","Heatmaps");
define("CALL_TEXT_13","We provide heatmaps showing the level of gluten free supply in any city around the world.");

define("CALL_TITLE_23","New gluten free offers");
define("CALL_TEXT_23","We monitor periodically the restaurants that include gluten fee options in their offer.");

define("CALL_TITLE_33","Business contacts");
define("CALL_TEXT_33","We provide list of these places including name, address and others public contacts available online");

define("TRY_NOW","Provalo, scopri dove mangiare senza glutine a...");

// FORM
define("PAGE_ALERT","Startup offer: fill the form to receive at the special price of €50 + VAT the list of restaurants in Rome and Milan that are currently offering gluten free options! The submission is not mandatory, you'll receive further instrucions.");
define("NAME","Full name");
define("EMAIL","E-mail");
define("COMPANYNAME","Company");
define("ADDRESS","Address");
define("ZIP","Zip");
define("CITY","City");
define("PRO","Province");
define("COUNTRY","Country");
define("PHONE","Phone number");
define("WEBSITE","Website");
define("MESSAGE","Message");
define("SEND_FORM","Send form");

define("FIELD_REQUIRED","Field required");