<?php
/*
------------------
Language: Italiano
------------------
*/
 
// META PAGE, DO NOT CHANGE CONSTANTS NAMES
define("META_TITLE", "Gluten free advisor");
define("META_DESCRIPTION", "Gastromama ti fa scoprire locali senza glutine ovunque nel mondo ed in pochi istanti, inclusi quelli non inseriti in appositi elenchi");
// END META PAGE

// CUSTOM PAGE TEXT HERE BELOW

$lang = array();
 
$lang['SITE_NAME'] = 'Gastromama';
$lang['PAGE_TITLE'] = 'Benvenuti su Gastromama';

$lang['HEADER_TITLE'] = 'Gastromama, il gluten free advisor';
$lang['SLOGAN'] = 'Aiutiamo le <a href="#users" style="font-weight:600">persone</a> a mangiare senza glutine ovunque, e le <a href="#business" style="font-weight:600">aziende</a> ad ottimizzare le strategie nel settore gluten free';


// USERS //

$lang['HEADING_TITLE'] = "Aiutiamo le persone a mangiare senza glutine ovunque";
$lang['HEADING'] = "Gluten free Advisor di Gastromama è una app che analizza le conversazioni in rete per scoprire i locali frequentati da chi come te mangia senza glutine e li localizza sul tuo cellulare in pochi istanti.";

define("SEARCH_TITLE", "Locali gluten free scovati di recente");


define("CALL_TITLE_1","Diverso");
define("CALL_TEXT_1","Non l'ennesima app con indirizzi di locali senza glutine, ma uno strumento che analizza le conversazioni  in rete come te per localizzare locali frequentati da chi mangia senza glutine, ovunque siano.");

define("CALL_TITLE_2","Magico");
define("CALL_TEXT_2","Funziona dappertutto, nella tua città così come dall'altra parte del mondo. Installalo e non ne potrai più fare a meno. Potrai visualizzare sia la posizione dei locali che altre info utili (se presenti) quali telefono e il sito, e tracciare il percorso col GPS del tuo cellulare.");

define("CALL_TITLE_3","Veloce");
define("CALL_TEXT_3","Dall'avvio dell'app alla visualizzazione dei locali intorno a te passano pochissimi secondi.");

/* BUSINESS */

$lang['HEADING_TITLE2'] = "Aiutiamo le aziende ad ottimizzare le strategie nel settore gluten free";
$lang['HEADING2'] = "La nostra tecnologia elabora dati pubblici e li trasforma in informazioni utili ad ottimizzare le strategie di mercato delle aziende che offrono prodotti e formazione nel mercato gluten free. Tutti i locali trovati sono potenziali clienti. ";

define("CALL_TITLE_12",">53.000");
define("CALL_TEXT_12","I locali con offerte gluten free scovati sino ad ora in tutto il mondo");

define("CALL_TITLE_22","80%");
define("CALL_TEXT_22","E' la quota di ristoranti a Roma, Milano e Napoli che hanno opzioni gluten free senza offrire garanzie su contaminazione e preparazione dei cibi, e che hanno bisogno di ampliare l'offerta e formare lo staff.");

define("CALL_TITLE_32","5%");
define("CALL_TEXT_32","Sono i ristoranti nei quali è possibile trovare opzioni gluten free nelle principali città italiane.");



define("CALL_TITLE_13","Mappe termiche");
define("CALL_TEXT_13","Mappiamo sul territorio il livello dell'offerta gluten free, per scovare le zone con più o meno conconcorrenza");

define("CALL_TITLE_23","Monitoraggio del mercato");
define("CALL_TEXT_23","Il numero di esercizi che introducono opzioni gluten free sono in costante aumento, ed hanno bisogno  di formazione/informazione in materia di contaminazione, e di ampliare o migliorare la qualità loro offerta attraverso prodotti o materie prime di qualità.");

define("CALL_TITLE_33","Indirizzi e contatti");
define("CALL_TEXT_33","Generiamo gli elenchi di questi locali, includendone indirizzo e contatti pubblici disponibili (telefoni, sito internet, indirizzo ecc.");

define("TRY_NOW","Provalo, scopri dove mangiare senza glutine a...");

// FORM
define("PAGE_ALERT","Compila il modulo per richiedere al prezzo di €50 + iva l'elenco dei ristoranti con opzioni senza glutine a Roma, Milano e Napoli! L'invio non è impegnativo e riceverai ulteriori informazioni.");
define("NAME","Nome completo");
define("EMAIL","E-mail");
define("COMPANYNAME","Società");
define("ADDRESS","Indirizzo");
define("ZIP","CAP");
define("CITY","Città");
define("PRO","Pro");
define("COUNTRY","Nazione");
define("PHONE","Telefono");
define("WEBSITE","Sito web");
define("MESSAGE","Messaggio");
define("SEND_FORM","Invia modulo");

define("FIELD_REQUIRED","Campo obbligatorio");