// GRAFICI
// g[n]              : objs grafici per lo scalare n (n=>1);
// g[n].annotations(): punti evidenzionati in g[n];
// myLabels[n]       : nomi delle serie presenti nella componente n ordinati secondo matrice valori myData[n];
// myData[n]         : matrici dei dati per lo scalare n; 
//                     es: myData[1][date][0] -> lettura  comp <1> dello strum <0> in data <date>
// myIDs[n]          : matrice con gli ID delle letture della componente n;
//                     es: myIDs[1][date][serie] -> ID lettura comp <1> dello strum <serie> in data <date>
// myVal[n]          : matrice con interi delle azioni eseguite sulla lettura (Val:1,Rif:2,BaseRef:4,Pub:8);
//                     es: myVal[1][date][serie] = 9 -> lettura comp <1> strum <serie> del <date> valida e pubblicata (1+8)
// myInstr           : array con gli strumenti presenti nella sessione

$(document).ready(function () {
    var scalars;
    var g = [];
    var myData;
    var myLabels;
    var myInstr = [];
    var seriesOpt = [];
    var annotations = [];
    var saveBg, saveCol;
    var last_ann = 0;
    var gSelectors = [];
    var index;
    var myIDs;

    myData = [];
    myLabels = [];
    myIDs = [];

    // CALL PER GLI SCALARI PRESENTI NEL SET STRUMENTI
    getScalars()

    // GENERO UN GRAFICO PER OGNI SCALARE
    $.each(scalars, function (index, scalar) {
        n = scalar.id_scalar; // imposto n come indice del grafico
        myData[n] = []; //inizializzo la matrice valori della componente
        seriesOpt[n] = []; //inizializzo le opzioni delle serie della componente
        myIDs[n] = []; //inizializzo la matrice degli ID di questa componente
        $("div.boxChart").append("<div id='scalar" + n + "' data-index='" + n + "' style='width:100%; height:200px;'></div>");
        gSelectors[n] = $("#scalar" + n);

        // CALL PER I DATI
        var msdata = getData();

        //POPOLO LA MATRICE LABELS COME OGGETTO JS
        creaLabels(n, msdata.lstinst);

        //POPOLO LA MATRICE VALORI COME OGGETTO JS
        var values = msdata.data;
        $.each(values, function (date, series) {
            var myrow = [];
            myIDs[n][date] = [];
            myrow.push(new Date(date));

            //SERIE DATI
            $.each(series, function (index, serie) {
                if (serie.val) {
                    myrow.push(serie.val);
                    myIDs[n][date][index] = serie.id;
                } else {
                    myrow.push(null);
                }
            });

            //SERIE BACKUP
            $.each(series, function (index, serie) {
                if (serie.val) {
                    myrow.push(serie.val)
                } else {
                    myrow.push(null);
                }
            });

            myData[n].push(myrow);
        });
        console.log("myLabels", myLabels);
        console.log("myData", myData);
        console.log("myIDs", myIDs);
        draw(index, n, myData[n], seriesOpt[n], scalar)
    });

    // Assegno i colori della serie agli strumenti nel box
    markSeries();
    // Sincronizzo i grafici
    var sync = Dygraph.synchronize(g, {
        zoom: true,
        selection: true,
        range: false
    });


// ######################## FUNZIONI ################################# 
    function getScalars() {
        $.ajax({
            url: "/API/comp/search?IDStrumenti=" + varGet['inst'],
            dataType: 'json',
            async: false,
            success: function (respScalars) {
                scalars = respScalars;
            }
        });

    }    // chiamata API per memorizzare in <scalars> tutti gli scalari del set strumenti
    function getData() {
        var myData;
        var myUrl = "/API/chart/msdata?out=json&IDInst=" + varGet['inst'] + "&id_scalar=" + n + "&from=2014-07-01";
        $.ajax({
            url: myUrl,
            dataType: 'json',
            async: false,
            success: function (respMsdata) {
                myData = respMsdata
            }
        });
        return myData
    }       // chiamata API per memorizzare in <myData[n]> le letture dello scalare <n> del set strumenti
    function markSeries() {
        console.log("myInstr", myInstr);
        $.each(g, function (n, graph) {
            //console.log(graph);
            $.each(myInstr, function (index, inst) {
                //console.log(graph.getPropertiesForSeries(inst).color);
                var inf = graph.getPropertiesForSeries(inst);
                if (inf) {
                    $('li[name=' + inst + ']').css("border-left", "8px solid " + inf.color)
                }
            });

        });
    }   // collega i colore delle etichette strumenti a quelli della serie dei grafici
    function creaLabels(x, jLabels) {
        myLabels[x] = ["Data"]
        $.each(jLabels, function (index, sName) {
            myLabels[x].push(index);
            if (myInstr.indexOf(index) == -1) {
                $("ul.lstInst").append("<li name=\"" + index + "\">\n\
                                    <i class=\"ion ion-radio-waves\"></i>\n\
                                    <span class=\"text\">" + index + "</span>\n\
                                    <div class='pull-right'><input id=\"Vis" + index + "\" data-name=\"" + index + "\" type=\"checkbox\" checked data-toggle=\"toggle\" data-size=\"mini\"></div>\n\
                                    <div class=\"tools\">\n\
                                        <i class=\"fa fa-power-off\" data-toggle=\"tooltip\" title=\"Salva e rilascia strumento\"></i>\n\
                                    </div>\n\
            </li>");

                myInstr.push(index);
            }
            $('li div input:checkbox').bootstrapToggle();

        });
        $.each(myLabels[x], function (index, lbl) {
            if (index > 0) {
                myLabels[x].push("_" + lbl)
                seriesOpt[x]["_" + lbl] = {
                    drawPoints: false,
                    strokeWidth: 0.1,
                    highlightCircleSize: 2
                }
            }
        });
    }  //genera gli array delle etichette di n grafici in myLabel[n]
    function draw(index, n, data, mySeriesOpt, scalar) {
        g[index] = new Dygraph(document.getElementById("scalar" + n),
                data,
                {
                    labels: myLabels[n],
                    title: scalar.comp_scalar,
                    ylabel: scalar.mu_name,
                    errorBars: false, // DEVIAZIONE STANDARD
                    showRangeSelector: true,
                    showInRangeSelector: false,
                    rangeSelectorHeight: 15,
                    rangeSelectorBackgroundLineWidth: 0.3,
                    rangeSelectorForegroundLineWidth: 0.3,
                    rangeSelectorForegroundStrokeColor: "khaki",
                    rangeSelectorForegroundStrokeColor: "khaki",
                    connectSeparatedPoints: true,
                    drawGrid: false,
                    drawCallback: function (g) {
                        var ann = g.annotations();
                    },
                    pointClickCallback: function (event, p) {
                        console.log(p);

                        // IGNORA LA SERIE DI BACKUP
                        if (p.name.substr(0, 1) == "_") {
                            valPoint(index, p.idx, p.name)
                            return;
                        } else {
                            invPoint(index, p.idx, p.name);
                        }
                        //g[index].updateOptions({'file': data[index]}, true);
                        //SELEZIONE DEL PUNTO
//                        if (p.annotation)
//                            return;
//                        var ann = {
//                            series: p.name,
//                            xval: p.xval,
//                            shortText: "X",
//                            text: "Clicca per rimuovere",
//                            cssClass: 'myAnnotation',
//                            tickHeight: -7
//                        };
//                        var anns = g[index].annotations();
//                        anns.push(ann);
//                        g[index].setAnnotations(anns);
                        //. SELEZION DEL PUNTO
                    },
                    highlightCallback: function (e, x, pts, row) {},
                    unhighlightCallback: function (e) {},
                    annotationMouseOverHandler: function (ann, point, dg, event) {
                        saveBg = ann.div.style.backgroundColor;
                        saveCol = ann.div.style.color;
                        ann.div.style.backgroundColor = ann.div.style.color;
                        ann.div.style.color = "white";
                    },
                    annotationMouseOutHandler: function (ann, point, dg, event) {
                        ann.div.style.backgroundColor = saveBg;
                        ann.div.style.color = saveCol;
                    },
                    annotationClickHandler: function (ann, point, dg, event) {
                        var anns = g[index].annotations();
                        removeAnn(g[index], anns, ann);
                    },
                    legendFormatter: function (data) {
                        var legendSeries = []
                        for (i = 0; i < (myLabels[n].length - 1) / 2; i++) {
                            legendSeries.push(data.series[i])
                        }

                        data.series = legendSeries;  // [data.series[0], data.series[1]] pick whichever series you want to keep
                        return Dygraph.Plugins.Legend.defaultFormatter.call(this, data);
                    },
                    drawPoints: true,
                    pointSize: 2,
                    //per-series options http://dygraphs.com/gallery/#g/per-series   
                    series: mySeriesOpt,
                    //per-axis options http://dygraphs.com/per-axis.html  
                    axes: {
                        x: {
                            axisLabelFormatter: function (x) {
                                return x.getDate() + "/" + x.getMonth() + "/" + x.getFullYear().toString().substr(-2);
                            }
                        },
                        y: {
                            axisLabelFormatter: function (y) {
                                return y;
                            }
                        }
                    }
                });


        function removeAnn(g, MainAnns, DelAnn) {
            for (var k in MainAnns) {
                if (MainAnns.hasOwnProperty(k)) {
                    var anns = g.annotations();
                    if (MainAnns[k].series + MainAnns[k].xval == DelAnn.series + DelAnn.xval) {
                        anns.splice(k, 1)
                        g.setAnnotations(anns);
                        break;
                    }
                }
            }

        }



    } //disegna il grafico della componente <n>
    function invPoint(comp, x, sname) {
        console.log(n, x, g[comp].indexFromSetName(sname));
        console.log(myData[comp + 1])
        myData[comp + 1][x][g[comp].indexFromSetName(sname)] = null;
        gReload(comp + 1);
    } // Invalida il punto <x> dello scalare <comp> dello strumento <sname>
    function valPoint(comp, x, bckSerie) {
        console.log("Rivalido " + bckSerie.substring(1) + " leggendo valore da " + bckSerie)
        myData[comp + 1][x][g[comp].indexFromSetName(bckSerie.substring(1))] = myData[comp + 1][x][g[comp].indexFromSetName(bckSerie)];
        gReload(comp + 1);
    } // Rivalida il punto <x> dello scalare <comp> dello strumento <Serie>
    function gReload(x) {
        g[x - 1].updateOptions({'file': myData[x]}, false);
    }   // Ricarica il grafico <n> dopo una modifica a myData[n]

// ######################## LISTENERS ################################# 
    $('li.cmdRestore').click(function () {
        data = []
        data = loadData()
        g.updateOptions({'file': data}, false);
        g.setAnnotations([])
    });
    $('li.cmdInvalida').click(function () {
        // PER IL MOMENTO INVALIDO I MARKER SU TUTTI I GRAFICI
        $.each(g, function (index, chart) {
            //console.log("Lavoro sul grafico ", index)
            var anns = g[index].annotations();
            var series = g[index].getLabels()
            //console.log("anns", anns)
            // per ogni punto marcato

            for (var k in anns) {
                // trova il num colonna nell'array delle labels (Y)
                if (anns.hasOwnProperty(k)) {
                    var y = g[index].getLabels().indexOf(anns[k]['series'])
                    //console.log("Y=" + y)
                    for (var x in data[index]) {
                        if (new Date(data[index][x][0]).getTime() == new Date(anns[k]['xval']).getTime()) {
                            //console.log("X=" + x)
                            data[index][x][y] = null
                            g[index].updateOptions({'file': data[index]}, false);
                            g[index].setAnnotations([])
                            break;
                        }
                    }
                }
            }
        });

    });
    $('input[data-toggle=toggle]').change(function () {

        var strumento = $(this).attr("data-name");
        var bckstrumento = "_" + strumento;
        console.log(strumento,bckstrumento)
        var val = $(this).prop('checked');
        
        $.each(myLabels, function (index, value) {
            if (value != undefined) {
                
                nserie = value.indexOf(strumento) - 1;
                if (nserie > -1) {
                    g[index - 1].setVisibility(nserie, val);
                }
                
                nserie = value.indexOf(bckstrumento) - 1;
                if (nserie > -1) {
                    g[index - 1].setVisibility(nserie, val);
                }
            }

        });

    })



});
