<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark control-sidebar-open" style="width:280px;background-color: #3c8dbc;height:100%">
    <!-- Tab panes -->
    <div class="tab-content"  style="padding: 50px auto 20px auto">
        <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
            <div>
                <div class="box box-default box-solid">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-line-chart"></i>

                        <h3 class="box-title">Strumenti</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <ul class="todo-list ui-sortable lstInst">

                        </ul>
                    </div>
                    <!-- /.box-body -->

                </div>

                <div class="box box-default box-solid">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-wrench"></i>

                        <h3 class="box-title">Azioni</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
                            <li>
                                <span class="text">Valida</span>
                                <div class='pull-right'><input type="checkbox" checked data-toggle="toggle" data-size="mini"></div>
                            </li>
                            <li>
                                <span class="text">Riferimento</span>
                                <div class='pull-right'><input type="checkbox" checked data-toggle="toggle" data-size="mini"></div>
                            </li>

                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="button" data-toggle="tooltip" title="Applica le azioni scelte ai punti selezionati" class="btn btn-block btn-default pull-right"><i class="fa fa-edit"></i> Applica</button>
                    </div>
                </div>
                <div style='display: flow-root;'>
                <button type="button" data-toggle="tooltip" title="Ripristina intera sessione" class="btn btn-lg btn-default  pull-left" style="margin-right:2px"><i class="fa fa-undo"></i></button>
                <button type="button" data-toggle="tooltip" title="Salva e rilascia sessione" class="btn btn-lg btn-warning  pull-right"><i class="fa fa-power-off"></i></button> 
                <button type="button" data-toggle="tooltip" title="Salva le modifiche" class="btn btn-lg btn-default  pull-left"><i class="fa fa-save"></i> </button>
                </div>
            </div>
        </div>
        <!-- /.tab-pane -->

        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->