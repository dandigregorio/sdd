
$(document).ready(function () {

    // 0) Init
    var myGlobal, myValues, icoClass;
    var chartValues = [];
    var chartName = [];
    var gs = []; //Grafici
    var table = $('#tbl-installazioni').DataTable({
        columns: [
            {data: "inst_id"}, // ID
            {data: "inst"}, // Strumento
            {data: "status",
                render: function (data, type, full, meta) {
                    // Se devo visualizzare formatto il dato...
                    if (type === "display") {
                        if (data == "Attivo") {
                            return '<i class="fa fa-toggle-on" aria-hidden="true"></i>';
                        } else if (data == "Inattivo") {
                            return '<i class="fa fa-toggle-off" aria-hidden="true"></i>';
                        } else {
                            return '<i class="fa fa-question-circle" aria-hidden="true"></i>';
                        }
                        // Altrimenti lo restituisco così com'è (per il sorting)    
                    } else {
                        return data;
                    }


                }


            }, // Status
            {data: "edited", render: function (data, type, full, meta) {
                    if (data) {
                        var Split = data.split(' ');
                        var dataSplit = Split[0].split('-');

                        var oraSplit = Split[1].split(':');

                        return dataSplit[2] + "/" + dataSplit[1] + "/" + dataSplit[0].substr(2, 2) + " " + oraSplit[0] + ":" + oraSplit[1];
                    } else {
                        return "";
                    }
                }}, // Editato
            {data: "published", render: function (data, type, full, meta) {
                    if (data) {
                        var Split = data.split(' ');
                        var dataSplit = Split[0].split('-');

                        var oraSplit = Split[1].split(':');

                        return dataSplit[2] + "/" + dataSplit[1] + "/" + dataSplit[0].substr(2, 2) + " " + oraSplit[0] + ":" + oraSplit[1];
                    } else {
                        return "";
                    }
                }}, // Pubblicato
            {data: "delta",
                render: function (data, type, full, meta) {
                    return data;
                }}, // Delta
            {data: "lockstatus", render: function (data, type, full, meta) {
                    if (type === "display") {

                        if (data == "AUTO") {
                            return '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
                        } else if (data == "INSESSION") {
                            return '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                        } else if (data == "") {
                            return '';
                        } else {
                            return "?"
                        }
                    } else {
                        return data
                    }


                }
            }, // Blocco
            {data: "auto",
                render: function (data, type, full, meta) {
                    if (type === "display") {
                        if (data == 0) {
                            return '<i class="fa fa-toggle-on" aria-hidden="true"></i>';
                        } else if (data == 1) {
                            return '<i class="fa fa-toggle-off" aria-hidden="true"></i>';
                        } else {
                            return '<i class="fa fa-question-circle" aria-hidden="true"></i>';
                        }
                    } else {
                        return data
                    }
                }}, // Auto
            {data: "valCounts",
                "orderable": false,
                render: function (data, type, full, meta) {
                    if (data > 0) {
                        icoClass = 'chartEnabled';
                        toggleMeta = 'data-toggle="modal" data-target="#modal-chart"';
                    } else {
                        icoClass = 'chartDisabled';
                        toggleMeta = '';
                    }
                    return '<i class="fa fa-area-chart icoChart ' + icoClass + '" data-sensor-values="' + data + '" data-sensor-id="' + full.inst_id + '" ' + toggleMeta + '></i>';
                }}, // Graph
            {data: "set",
                "orderable": false,
                render: function (data, type, full, meta) {
                    if (full.valCounts > 0) {
                        icoClass = 'setEnabled';
                    } else {
                        icoClass = 'setDisabled';
                    }
                    return '<i class="fa fa-th icoSet ' + icoClass + '" data-sensor-name="' + full.inst + '" data-sensor-id="' + full.inst_id + '"></i>'
                }}   // Sess
        ],
        language: {
            "sEmptyTable": "Nessuno strumento da visualizzare",
            "sInfo": "Vista da _START_ a _END_ di _TOTAL_ strumenti",
            "sInfoEmpty": "Vista da 0 a 0 di 0 strumenti",
            "sInfoFiltered": "(filtrati da _MAX_ strumenti in totale)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Visualizza _MENU_ strumenti",
            "sLoadingRecords": "Caricamento...",
            "sProcessing": "Elaborazione...",
            "sSearch": "Filtra:",
            "sZeroRecords": "La ricerca non ha portato alcun risultato.",
            "oPaginate": {
                "sFirst": "Inizio",
                "sPrevious": "Precedente",
                "sNext": "Successivo",
                "sLast": "Fine"
            },
            "oAria": {
                "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
            }

        }
    });
    init()
    var sessBox = $('.session .bootstrap-tagsinput input');
    sessBox.tagsinput({
        itemValue: 'inst_id',
        itemText: 'inst'
    });

    var opedTagInput = $('.oped  input');
    var tstTagInput = $('.tst  input');
    var numTagInput = $('.num  input');
    var posTagInput = $('.pos  input');

    // Aggiunta e rimozione tags di ricerca
    $('.search .bootstrap-tagsinput input').on('change', function (event) {
        var searchUrl = '/API/inst/labels/search?1-5=' + opedTagInput.val() + '&6-2=' + tstTagInput.val() + '&8-2=' + numTagInput.val() + '&10-1=' + posTagInput.val();
        $("#box_table").append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
        table.ajax.url(searchUrl).load();
    });

    var opeds = new Bloodhound({
        datumTokenizer: function (d)
        {
            var tokens = [];
            //the available string is 'name' in your datum
            var stringSize = d.label.length;
            //multiple combinations for every available size
            //(eg. dog = d, o, g, do, og, dog)
            for (var size = 1; size <= stringSize; size++) {
                for (var i = 0; i + size <= stringSize; i++) {
                    tokens.push(d.label.substr(i, size));
                }
            }

            return tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/API/inst/labels.php?start=1&len=5',
        limit: 10
    });
    opeds.initialize();
    opedTagInput.tagsinput({
        itemValue: 'label', // valore del tag
        itemText: 'label', // testo del tag
        hint: true,
        typeaheadjs: {
            displayKey: 'label', //il campo da visualizzare nei suggerumenti
            source: opeds.ttAdapter()
        }
    });

    var tstnames = new Bloodhound({
        datumTokenizer: function (d)
        {
            var tokens = [];
            //the available string is 'name' in your datum
            var stringSize = d.label.length;
            //multiple combinations for every available size
            //(eg. dog = d, o, g, do, og, dog)
            for (var size = 1; size <= stringSize; size++) {
                for (var i = 0; i + size <= stringSize; i++) {
                    tokens.push(d.label.substr(i, size));
                }
            }

            return tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/API/inst/labels.php?start=6&len=2',
        limit: 10
    });
    tstnames.initialize();
    tstTagInput.tagsinput({
        itemValue: 'label', // valore del tag
        itemText: 'label', // testo del tag
        hint: true,
        typeaheadjs: {
            displayKey: 'label', //il campo da visualizzare nei suggerumenti
            source: tstnames.ttAdapter()
        }
    });

    var numnames = new Bloodhound({
        datumTokenizer: function (d)
        {
            var tokens = [];
            //the available string is 'name' in your datum
            var stringSize = d.label.length;
            //multiple combinations for every available size
            //(eg. dog = d, o, g, do, og, dog)
            for (var size = 1; size <= stringSize; size++) {
                for (var i = 0; i + size <= stringSize; i++) {
                    tokens.push(d.label.substr(i, size));
                }
            }

            return tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/API/inst/labels.php?start=8&len=2',
        limit: 10,
        freeInput: false
    });
    numnames.initialize();
    numTagInput.tagsinput({
        itemValue: 'label', // valore del tag
        itemText: 'label', // testo del tag
        hint: true,
        typeaheadjs: {
            displayKey: 'label', //il campo da visualizzare nei suggerumenti
            source: numnames.ttAdapter()
        }
    });

    var posnames = new Bloodhound({
        datumTokenizer: function (d)
        {
            var tokens = [];
            //the available string is 'name' in your datum
            var stringSize = d.label.length;
            //multiple combinations for every available size
            //(eg. dog = d, o, g, do, og, dog)
            for (var size = 1; size <= stringSize; size++) {
                for (var i = 0; i + size <= stringSize; i++) {
                    tokens.push(d.label.substr(i, size));
                }
            }

            return tokens;
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/API/inst/labels.php?start=10&len=1',
        limit: 10,
        freeInput: false
    });
    numnames.initialize();
    posTagInput.tagsinput({
        itemValue: 'label', // valore del tag
        itemText: 'label', // testo del tag
        hint: true,
        typeaheadjs: {
            displayKey: 'label', //il campo da visualizzare nei suggerumenti
            source: posnames.ttAdapter()
        }
    });

    // ELIMINA IL DIV DOPPIO SU .bootstrap-tagsinput
    $("div.bootstrap-tagsinput div.bootstrap-tagsinput").unwrap();

    // 3a) Collega la modale all'ID del sensore cliccato    
    $('#tbl-installazioni tbody').on('click', 'td', function () {
        //console.log(table.row(this).data().inst_id);
        $("#modal-chart").attr("data-sensor-id", table.row(this).data().inst_id);
        $("#modal-chart .modal-title").text("Rilevazioni " + table.row(this).data().inst);
    });

    // 3b) Inizializza la modale 
    $('#tbl-installazioni tbody').on('click', 'i.chartEnabled', function () {
        $("#modal-chart .modal-title").text($(this).attr('data-sensor-id'));
        $("#modal-chart .modal-body").html("");
        gs = [];
    });

    // 4) Genera i grafici singoli
    $('#modal-chart').on('shown.bs.modal', function (e) {
        var inst_id = $(this).attr("data-sensor-id");
        // Rileva i canali del sensore selezionato
        $.getJSON("/API/comp/search?IDStrumenti=" + inst_id, function (data) {
            myScalars = data;
            console.log("myScalars", myScalars)
            // per ogni canale ...
            $.each(myScalars, function (index, scalar) {
                chartName[scalar.id_scalar] = "chart" + scalar.id_scalar;
                $("#modal-chart .modal-body").append('<div id="' + chartName[scalar.id_scalar] + '" style="width:90%; height:220px; margin-bottom:20px"></div>')
                // chiamo le letture
                var csvPath = "/API/chart/sdata?out=csv&IDStrumento=" + inst_id + "&IDScalar=" + scalar.id_scalar;
                console.log(csvPath);
                gs.push(new Dygraph(
                        document.getElementById(chartName[scalar.id_scalar]),
                        csvPath, // path to CSV file
                        {
                            legend: 'always',
                            labels: ["Data", scalar.mu],
                            title: scalar.comp_scalar,
                            showRangeSelector: true,
                            ylabel: scalar.mu_name,
                            rangeSelectorHeight: 12
                        }
                ));
                // . chiamo le letture
            });
            // . per ogni canale ...
            var sync = Dygraph.synchronize(gs, {
                zoom: true,
                selection: true,
                range: false
            });
        });
        // Linka i grafici

    })

    // 5) Cancella grafici singoli in chiusura
    $('#modal-chart').on('hide.bs.modal', function (e) {
        $(this).attr("data-sensor-id", "");
        $(".modal-title").text("Opps...");
        $(".modal-body").html("");

    })

    // 5a) Cancella grafici multiserie in chiusura
    $('#modal-setchart').on('hide.bs.modal', function (e) {
        $(this).attr("data-sensor-id", "");
        $("#modal-setchart .modal-title").text("Opps...");
        $("#modal-setchart .modal-body").html('<div id="demo"></div>');

    })

    // Apertura modale multiserie
    $('#modal-setchart').on('shown.bs.modal', function (e) {
        $("#modal-setchart .modal-title").text(sessBox.val());
        $.each(sessBox.tagsinput('items'), function (index, value) {
            console.log(value);
        });
        $.getJSON("/API/comp/search?IDStrumenti=" + sessBox.val(), function (data) {
            var json, numComp;
            json = data
            numComp = json.lenght
            console.log("TotScalari", json);
            var makeGraph = function (className, numSeries, numRows, isStacked, scalars) {
                var demo = document.getElementById('demo');
                var div = document.createElement('div');
                var labels = [];
                var mylabels = [];
                var myValues = [];
                div.className = className;
                div.style.display = 'inline-block';
                div.style.margin = '20px';
                demo.appendChild(div);

                var csvPath = "/API/chart/msdata?out=csv&IDInst=" + sessBox.val() + "&id_scalar=" + scalars.id_scalar
                console.log("csvPath", csvPath)


                gs.push(new Dygraph(
                        div,
                        csvPath,
                        {
                            width: 480,
                            height: 200,
                            legend: 'always',
                            title: scalars.comp_scalar,
                            ylabel: scalars.mu_name,
                            showRangeSelector: true,
                            highlightCircleSize: 2,
                            strokeWidth: 1,
                            strokeBorderWidth: isStacked ? null : 1,
                            rangeSelectorHeight: 12,
                            connectSeparatedPoints:true,
                            highlightSeriesOpts: {
                                strokeWidth: 2,
                                strokeBorderWidth: 1,
                                highlightCircleSize: 4
                            }
                        }));

//                    var onclick = function (ev) {
//                        if (g.isSeriesLocked()) {
//                            g.clearSelection();
//                        } else {
//                            g.setSelection(g.getSelection(), g.getHighlightSeries(), true);
//                        }
//                    };
//                    g.updateOptions({clickCallback: onclick}, true);
//                    g.setSelection(false, 's005');
            };

            $.each(json, function (x, row) {
                makeGraph("many", 10, 10, true, row);
            });
            var sync = Dygraph.synchronize(gs, {
                zoom: true,
                selection: true,
                range: false
            });

        });


    })

    //################### EVENT LISTENERS ###################

    // Aggiunta strumento in nuova sessione
    $('#tbl-installazioni tbody').on('click', 'i.setEnabled', function () {
        sessBox.tagsinput('add', {"inst_id": $(this).attr('data-sensor-id'), "inst": $(this).attr('data-sensor-name')});
    });

    // Aggiunta e rimozione strumenti da nuova sessione
    sessBox.change(function () {
        if ($(this).val()) {
            $("#boxSet button").prop("disabled", false);
        } else {
            $("#boxSet button").prop("disabled", true);
        }
    });

    // Gestione click su Enter
    $(document).on("keypress", ":input:not(textarea)", function (event) {
        return event.keyCode != 13;
    });

    // Evento tabella ricaricata
    table.on('draw.dt', function () {
        $("#box_table .overlay").remove();
    });

    // Visibilità colonne
    $('a.toggle-vis').on('click', function (e) {
        e.preventDefault();

        // Get the column API object
        var column = table.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());

        $(this).css("font-color", "lightgrey")
    });

    // Rileva cambiamento visibilità colonne
    table.on('column-visibility.dt', function (e, settings, column, state) {
        if (state) {
            $("[data-column=" + column + "]").css("color", "black")
        } else {
            $("[data-column=" + column + "]").css("color", "lightgrey")
        }

    });

    $('.btnNewSession').click(function () {
        console.log("Click")
        window.location.href = "/page/dati/sessione/?inst=" + sessBox.val();
    })

    //################### FUNCTIONS ####################
    function init() {
        //Nasconde colonna ID da tabella
        table.column(0).visible(false);
        $("[data-column=0]").css("color", "lightgrey")
        //Carica tabella
        $("#box_table").append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
        table.ajax.url("/API/inst/labels/search").load();
        //Disabilita pulsanti sessione
        $("#boxSet button").prop("disabled", true)
    }



});