<!--Datatables for bootstrap-->
<!--<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" src="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js"></script>

<script src="/js/plugin/dygraph/dygraph.js"></script>
<script src="/js/plugin/dygraph/extras/synchronizer.js"></script>
<!--<link rel="stylesheet" src="/js/plugin/dygraph/dygraph.css" />-->

<!--Tags Input-->
<link rel="stylesheet" href="/js/plugin/tagsinput/dist/bootstrap-tagsinput.css">
<!--<link rel="stylesheet" href="/js/plugin/tagsinput/examples/assets/app.css">-->
<script src="/js/plugin/tagsinput/dist/bootstrap-tagsinput.min.js"></script>

