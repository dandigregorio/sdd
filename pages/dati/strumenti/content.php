   
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ricerca strumenti
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                <li class="">Dati</li>
                <li class="active">Strumenti</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">  

                    <div class="box box-primary">
                        <div class="box-header"><i class="ion ion-search"></i>
                            <h3 class="box-title">Ricerca</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body search">

                            <div class="col-md-4 oped" style="padding:3px;font-weight:300">
                                <span class="direct-chat-timestamp">Opera,sezione / Edificio</span>
                                <input type="text" data-role="tagsinput" />
                            </div>

                            <div class="col-md-3 tst" style="padding:3px;font-weight:300">
                                <span class="direct-chat-timestamp">Tipo strumento</span>
                                <input type="text" data-role="tagsinput" />
                            </div>


                            <div class="col-md-3 num" style="padding:3px;font-weight:300">
                                <span class="direct-chat-timestamp">Progressivo</span>
                                <input type="text" data-role="tagsinput" />
                            </div>

                            <div class="col-md-2 pos" style="padding:3px;font-weight:300">
                                <span class="direct-chat-timestamp">Posizione</span>
                                <input type="text" data-role="tagsinput" />
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- .SELEZIONE STRUMENTO -->

                    <div id="box_table" class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title col-md-3">Risultati</h3> 
                            <div class="col-md-9" style="text-align: right;padding-right: 0;">                        
                                <div class="btn-group">
                                    <a class="btn btn-default btn-xs toggle-vis" data-column="0">ID</a> 
                                    <a class="btn btn-default btn-xs toggle-vis" data-column="1">Strumento</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="2">Stato</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="3">Editato</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="4">Pubblicato</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="5">Delta</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="6">Blocco</a>
                                    <a class="btn btn-default btn-xs  toggle-vis" data-column="7">Auto</a>
                                </div></div>
                        </div>
                        <div class="box-body">



                            <table id="tbl-installazioni" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size:13px">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Strumento</th>
                                        <th>Status</th>
                                        <th>Editato</th>
                                        <th>Pubblicato</th>
                                        <th>Delta gg</th>
                                        <th>Blocco</th>
                                        <th>Auto</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Strumento</th>
                                        <th>Status</th>
                                        <th>Editato</th>
                                        <th>Pubblicato</th>
                                        <th>Delta gg</th>
                                        <th>Blocco</th>
                                        <th>Auto</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>


                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- .RISULTATI-->

                </div>
                <!-- /.col (FILTER) -->
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="ion ion-clipboard"></i>

                            <h3 class="box-title">Sessioni aperte</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                            <ul class="todo-list ui-sortable">
                                <li>
                                    <!-- drag handle -->
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <!-- todo text -->
                                    <span class="text">Mauro12536589</span>
                                    <!-- Emphasis label -->
                                    <small class="label label-danger"><i class="fa fa-clock-o"></i> 6 giorni</small>
                                    <!-- General tools such as edit or delete-->
                                    <div class="tools">
                                        <i class="fa fa-edit"></i>
                                        <i class="fa fa-trash-o"></i>
                                    </div>
                                </li>
                                <li>
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <span class="text">Pasquale5656897</span>
                                    <small class="label label-default"><i class="fa fa-clock-o"></i> 1 giorno</small>
                                    <div class="tools">
                                        <i class="fa fa-edit"></i>
                                        <i class="fa fa-trash-o"></i>
                                    </div>
                                </li>
                                <li>
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <span class="text">Alessandro25656599</span>
                                    <small class="label label-default"><i class="fa fa-clock-o"></i> 3 ore</small>
                                    <div class="tools">
                                        <i class="fa fa-edit"></i>
                                        <i class="fa fa-trash-o"></i>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->

                    </div>
                    <div id="boxSet" class="box box-default">
                        <div class="box-header"><i class="ion ion-wand"></i>
                            <h3 class="box-title">Nuova sessione</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body session">

                            <input name="session" type="text" data-role="tagsinput" />
                        </div>
                        <div class="box-footer">
                            <button  type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-setchart"><i class="fa fa-area-chart"></i> Grafico</button>
                            <button type="button" class="btn btn-default pull-right btnNewSession"><i class="fa fa-plus"></i> Apri</button>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col (FILTER) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <div class="modal fade" id="modal-chart">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-setchart">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Set Charts</h4>
                </div>
                <div class="modal-body"><div id="demo"></div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>