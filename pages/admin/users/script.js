// Pages::scan

$(document).ready(function () {
// create the editor
    var container = document.getElementById('jsoneditor');
    var options = {};
    var editor = new JSONEditor(container, options);
    $.getJSON('/backend/data/app.json', function (data) {
        editor.set(data);
    });

    // get json
    document.getElementById('saveJson').onclick = function () {
        var json = editor.get();
        $("#box_editor").append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
        $.ajax
                ({
                    type: "GET",
                    dataType: 'json',
                    async: false,
                    url: '/API/tools/jsave.php',
                    data: {data: JSON.stringify(json)},
                    success: function () {
                        alert("Thanks!");
                    },
                    failure: function () {
                        alert("Error!");
                    }
                });
        setTimeout(function(){ $(".overlay").remove(); }, 500);
    };
});