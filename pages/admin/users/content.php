<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Utenze e privilegi
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-unlock-alt"></i> Admin</a></li>
            <li class="active">Utenze e privilegi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-md-7">
            <div id="box_editor" class="box box-primary">
                <div class="box-header with-border"><i class="fa fa-cogs" aria-hidden="true"></i>
                    <h3 class="box-title">Configurazione</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Il file di configurazione contiente informazioni sugli utenti, sui ruoli e sui privilegi. Consultare la guida a destra per dettagli
              </div>
<div id="jsoneditor"></div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button id="saveJson" type="button" class="btn btn-default pull-right"><i class="fa fa-floppy-o"></i> Salva</button>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- Json editor -->
        <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header with-border"><i class="fa fa-question"></i>
                    <h3 class="box-title">Aiuto</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
<div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-default">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        Roles
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                        L'array "roles" contiene tutti i ruoli che è possibile assegnare agli utenti nel formato<br /><pre>ID:nomeruolo.</pre>
                        
                        Per aggiungere un ruolo duplicare l'ultimo inserito e cambiare il nome. L'ID è costituito dall'ordine di inserimento.
                    </div>
                  </div>
                </div>
                <div class="panel box box-default">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                        Privs
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                    <div class="box-body">
                        Contiene i privilegi di accesso a livello URL per il ruolo con lo stesso ID. Esempio:
              
                            <pre>
privs 
 0 <span style="color:lightgrey"><- ID ruolo</span>
   dati <span style="color:lightgrey"><-Gruppo privilegi x URL base /page/dati/</span>
     overall: 0 <span style="color:lightgrey"><- Wildcard per /page/dati/*</span>
     strum: 1 <span style="color:lightgrey"><- Privilegio x /page/dati/strum/</span>
     sess: 0 <span style="color:lightgrey"><- Privilegio x /page/dati/sess/</span></pre>
     Per aggiungere un privilegio duplicare l'ultimo elemento dell'elemento (id ruolo / URL base / subdir) e modificarne i valori
                        </dt>
                    </div>
                  </div>
                </div>
                <div class="panel box box-default">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                        Users
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                    <div class="box-body">
                      Utenti abiliati al login. Per creare un nuovo utente duplicare l'ultimo e modificarne i valori
                    </div>
                  </div>
                </div>
              </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.row (main row) -->
        </div>
    </section>
    <!-- /.content -->
</div></div>
<!-- /.content-wrapper -->
<style>

    div.jsoneditor-menu {
    width: 100%;
    height: 35px;
    padding: 2px;
    margin: 0;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    color: white;
    background-color: #00c0ef!important;
    border-bottom: 1px solid #00c0ef!important;
}

</style>    