<?php
header('Content-Type: text/xml');
$dirPath=$_SERVER['DOCUMENT_ROOT']."/APIs/cache/venues";
$baseUrl="http://".$_SERVER['HTTP_HOST']."/glutenfree/venue/";
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<?php
if ($handle = opendir($dirPath)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != ".." && $entry != ".json") {
            $x = explode(".json", $entry);
            $pageUrl = xml_entities($baseUrl.$x[0]."/");
            print  "<url>"
                    . "<loc>$pageUrl</loc>"
                    . "<changefreq>weekly</changefreq>"
                    . "</url>\n";
            $pageUrl ="";
        }
    }
    closedir($handle);
}
?>
<url>
<loc>http://gastromama.com/</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/London,%20United%20Kingdom/51.50853,-0.12574/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Rome,%20Italy/41.89193,12.51133/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Paris,%20France/48.85341,2.3488/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/New%20York,%20United%20States/40.71427,-74.00597/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Berlin,%20Germany/52.52437,13.41053/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Madrid,%20Spain/40.4165,-3.70256/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Istanbul,%20Turkey/41.01384,28.94966/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Barcelona,%20Spain/41.38879,2.15899/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Miami,%20United%20States/25.77427,-80.19366/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Amsterdam,%20Netherland/52.37403,4.88969/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Brussel,%20Belgium/50.85045,4.34878/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Prague,%20Czech%20Republic/50.08804,14.42076/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Budapest,%20Ungary/47.49801,19.03991/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Dublin,%20Ireland/53.33306,-6.24889/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Las%20Vegas,%20United%20States/36.17497,-115.13722/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/San%20Paolo,%20Brazil/-23.5475,-46.63611/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Buenos%20Aires,%20Argentina/-34.61315,-58.37723/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Vancouver,%20Canada/49.24966,-123.11934/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Toronto,%20Canada/43.70011,-79.4163/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>
http://gastromama.com/glutenfree/Sydney,%20Australia/-33.86785,151.20732/
</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>http://gastromama.com/page/how/</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>http://gastromama.com/page/contact/</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>http://gastromama.com/page/privacy/</loc>
<changefreq>weekly</changefreq>
</url>
<url>
<loc>http://gastromama.com/page/terms/</loc>
<changefreq>weekly</changefreq>
</url>
</urlset>

<?php
function xml_entities($string) {
    return strtr(
        $string, 
        array(
            "<" => "&lt;",
            ">" => "&gt;",
            '"' => "&quot;",
            "'" => "&apos;",
            "&" => "&amp;",
        )
    );
}
?>