<?php
//Richiede la variabile "matricola" con metodo GET
//--------
//Restituisce ruolo (0,1,2,3,4,5) e label ruolo (District Manager,Branch Manager, ecc)
//in formato Json con chiavi [role] e [label]       
//--------

//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

include_once($_SERVER['DOCUMENT_ROOT'].'/config/bootstrap.php'); 
$key = dec($_GET['key']);
//$key = $_GET['key'];
$out = $_GET['out'];

$sqlUser=sprintf("SELECT * FROM `users` WHERE `username` LIKE '%s'",$key);
$usrData['meta']['sql'] = $sqlUser;
$connUser = mysql_query($sqlUser,$dbConn) or die("$sqlCheck: ".mysql_error()); 
$row_user = mysql_fetch_assoc($connUser);
//
$usrData['data']['id']          = $row_user['id'];
$usrData['data']['firstname']   = $row_user['firstname'];
$usrData['data']['lastname']    = $row_user['lastname'];
$usrData['data']['gender']      = $row_user['gender'];
$usrData['data']['username']    = $row_user['username'];


// SET USER AVATAR
if ($row_user['fbid'] && $row_user['fbavatar']) {
    // if FB avatar is present
    $usrData['data']['avatar'] = $row_user['fbavatar'];}

elseif (file_exists ($_SERVER['DOCUMENT ROOT'].'/img/avatar/'.$row_user['id'].'.jpg')) {
    //elseif custom avatar is present
    $usrData['data']['avatar'] = '/img/avatar/'.$row_user['id'].'.jpg';}
else {
    //otherwise I catch the Gravatar
    // see more on https://en.gravatar.com/site/implement/hash/
    $defaultAvatar = "http://".$_SERVER['HTTP_HOST']."/img/icons/buddy.png";
    $myHash=md5(strtolower(trim($row_user['email'])));
    $usrData['data']['avatar'] = "http://www.gravatar.com/avatar/$myHash?s=200&d=$defaultAvatar";}

    
    
if ($out == 'json') {echo json_encode($usrData,64+128);} else {echo serialize($usrData);}


