<?php
$cacheDuration = 60*60*24;
$venueInfo = $_POST['venueInfo'];
$cachePath = $_SERVER['DOCUMENT_ROOT']."APIs/cache/venues/".$venueInfo['key'].".json";

if (    file_exists($cachePath) && (time() - filemtime($cachePath) < $cacheDuration)   ) {
    $response = array (
        'saved' => "cached",
        'age' => filemtime($cachePath),
        'expiration' => filemtime($cachePath) + $cacheDuration,
        'cachePath' => $cachePath,
        'venueInfo' => $venueInfo,
        'neighbourhoodPath' => $neighbourhoodPath
        );
    header('Content-Type: application/json; charset=UTF8');
    echo json_encode($response);
    exit;
}

$neighbourhoodPath = "http://".$_SERVER['HTTP_HOST']."/backend/geo2country.php?auth=kaos69&lat=".$venueInfo['lat']."&lng=".$venueInfo['lng'];

$neighbourhood  = json_decode(file_get_contents($neighbourhoodPath,1));
$venueInfo['neighbourhood'] = $neighbourhood;
file_put_contents($cachePath,json_encode($venueInfo));


if (file_exists($cachePath)) {$saved=true;} else {$saved=false;}

$response = array (
        'saved' => $saved,
        'age' => filemtime($cachePath),
        'expiration' => filemtime($cachePath) + $cacheDuration,
        'cachePath' => $cachePath,
        'venueInfo' => $venueInfo,
        'neighbourhoodPath' => $neighbourhoodPath
        );

header('Content-Type: application/json; charset=UTF8');
echo json_encode($response);
?>