<?php
//Controller per la verifica di login prima di redirect
//--------<
//Struttura Link
//<a href="<link dest>" authPage="<home pagina da verificare> class="<trigger js>">anchor</a> 
//Esempio: <a href="http://localhost/users/myplaces/" reqPage="users" class="getAuth">Create radar</a>      
//--------
//Output: 
// Json con auth = false se non autorizzato, auth = true se autorizzato

//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

include_once($_SERVER['DOCUMENT_ROOT'].'/config/bootstrap.php');
$reservedPages = array("users");

if (!in_array($_GET['reqPage'], $reservedPages)) {
    $jsonResp['auth'] = true;    
} else {
    if ($myuser->logged) {
        $jsonResp['auth'] = true;
    } else {
        $jsonResp['auth'] = false;
    }
}   

header('Content-Type: application/json'); 
echo json_encode($jsonResp)

?><?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

