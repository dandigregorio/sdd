<?php
//Richiede la variabile "matricola" con metodo GET
//--------
//Restituisce ruolo (0,1,2,3,4,5) e label ruolo (District Manager,Branch Manager, ecc)
//in formato Json con chiavi [role] e [label]       
//--------

//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

include_once($_SERVER['DOCUMENT_ROOT'].'/config/bootstrap.php'); 

$types = array("city"=>1,"radar"=>2,"mobile"=>3);

$type=$types[$_POST['type']];
$city=$_POST['city'];
$lang=$_POST['lang'];
$cookie_id = $_COOKIE['dvc_id'];
$user_id = $myuser->id;
$place_id = $_POST['place_id'];
$lat = mysql_real_escape_string(trim($_POST['lat']));
$lng = mysql_real_escape_string(trim($_POST['lng']));
$results = $_POST['results'];
$venues = mysql_real_escape_string(trim($_POST['venues']));

$sqlPush = sprintf("INSERT INTO `users_searches` (`type`, `cookie_id`, `user_id`, `place_id`, `lat`, `lng`, `results`,`venues` ) VALUES (%s, '%s', '%s', '%s', %s, %s, %s, '%s')",
            $type, $cookie_id, $user_id, $place_id, $lat, $lng, $results, $venues);

//$r['meta']['post'] = $_POST;
$r['meta']['sql'] = $sqlPush;

$sqlExec = mysql_query($sqlPush,$dbConn);
//
if (!$sqlExec) {
    $r['ok'] = false;
    $r['msg'] = "MySQL error";
    header('Content-Type: application/json');
    echo json_encode($r);
    exit;
}

if ($type == 1 && $results > 5) {
    $sPath = $_SERVER['DOCUMENT_ROOT'].'/APIs/cache/searches/'.$lang.'.json';
    if (file_exists($sPath)) {$recent = json_decode(file_get_contents($sPath),1);}
    $current_search = array('location'=>$city,'latlng'=>"$lat,$lng",'results'=>$results);
    if (!in_array($current_search,$recent,true)) { 
        array_unshift($recent,$current_search);
        //array_pop($recent);
    }    
    file_put_contents($sPath, json_encode($recent));
    $r['meta']['recent'] = $recent;
    $r['meta']['recentpath'] = $sPath;
}


$r['meta']['sqlResp'] = $sqlExec;
$r['ok'] = true;
$r['msg'] = "";

header('Content-Type: application/json');
echo json_encode($r);
exit;



?>
