<?php
//Richiede la variabile "matricola" con metodo GET
//--------
//Restituisce ruolo (0,1,2,3,4,5) e label ruolo (District Manager,Branch Manager, ecc)
//in formato Json con chiavi [role] e [label]       
//--------

//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

include_once($_SERVER['DOCUMENT_ROOT'].'/config/bootstrap.php'); 



$place_id = $_POST['place_id'];
$lat = mysql_real_escape_string(trim($_POST['lat']));
$lng = mysql_real_escape_string(trim($_POST['lng']));
$route = mysql_real_escape_string(trim($_POST['route']));
$locality = mysql_real_escape_string(trim($_POST['locality']));
$postal_code = mysql_real_escape_string(trim($_POST['postal_code']));
$country_short = mysql_real_escape_string(trim($_POST['country_short']));


        
$sqlCheckRadar = sprintf("INSERT INTO places (`place_id`, `lat`, `lng`, `route`, `locality`, `postal_code`, `country_short`)
        SELECT * FROM (SELECT '%s', %s, %s, '%s', '%s', '%s', '%s') AS tmp
        WHERE NOT EXISTS (
            SELECT `place_id` FROM `places` WHERE `place_id` = '%s'
        ) LIMIT 1;",
        $place_id, $lat, $lng, $route, $locality, $postal_code, $country_short, $place_id);

$r['meta']['post'] = $_POST;
$r['meta']['sql'] = $sqlCheckRadar;

$sqlExec = mysql_query($sqlCheckRadar,$dbConn);
if (!$sqlExec) {
    $r['ok'] = false;
    $r['msg'] = "MySQL error";
    header('Content-Type: application/json');
    echo json_encode($r);
    exit;
}



$r['meta']['sqlResp'] = $sqlExec;
$r['ok'] = true;
$r['msg'] = "";

header('Content-Type: application/json');
echo json_encode($r);
exit;



?>
