<?php 
// Main Controller for php scripts called via JS
// Usage example on JQuery script:
// 
// $.post("callController.php", {
//       "controller":name, ->call '/controllers/js2php/name.php'
//       "action": "auth",     ->specify a specific  method 
//       "var1": val1,         ->post parameters
//       "var2": val2           
//       }, function(data) {   
//              if(data == okmessage) {                          
//                      
//                      ....OK CODE .....
//                          
//                } else {
//                        ....ERROR CODE .....
//                        return false;
//                }
//                });
//
?>
<?php

//Load Appwide configuration file (already included in pages trough bootstrap.php)
include_once 'config/bootstrap.php'; 
//Call the controller specified in $_POST['controller'] value
include_once 'controllers/js2php/'.$_POST['controller'].'.php'; 
?>