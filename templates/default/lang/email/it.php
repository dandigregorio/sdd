<?php

/*------------------
Language: Italiano
------------------*/

// EMAIL
$email['sender_name'] = "Gastromama";
$email['sender_email'] = "noreply@gastromama.it";
$email['verification_subject'] = "Conferma la tua iscrizione a Gastromama";
$email['verification_body'] = "<p>Ciao %s,</p> 
            <p>è un piacere averti a bordo. per favore conferma la tua iscrizione cliccando sul link qui sotto:<br/>
            %s</p>
            <p>Grazie!</p> 
            <p>Gastromama staff</p>";