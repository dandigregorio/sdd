<?php

/*
------------------
Language: English
------------------
*/
 
// NAVBAR
define("HOW", "How it works");
define("CONTACT", "Contact us");
define("LOGIN", "Login");
define("CITYPICKER", "Enter a city");
define("ADD_PLACE", "Place another radar");
define("QUIT", "Quit");


// LOGIN/REGISTER FORM
define("FB_LOGIN","Login with Facebook");
define("FB_REGISTER","Register with Facebook");

define("ALTERNATIVE","or");
define("OR_MAIL","or login via email");

define("LOGIN","Login");
define("REGISTER","Register");

define("NOT_REGISTER","Still not registered?");
define("REGISTER NOW","Register now!");

define("REGISTERED","Already registered?");
define("LOGIN_NOW","Login here");

define("INSERT_EMAIL","Insert your email");
define("CHOOSE_PSW","Choose a password");
define("VERIFY_PSW","Confirm your password");

// FOOTER
define("PAGES","Pages");
define("BLOG_POST","Recent blog articles");
define("BLOG_LIST",'<ul class="footer-links">
                <li><a href="http://blog.gastromama.com/glute-free-restaurants-barcelona/">Best Gluten Free Restaurants and Tapas in Barcelona</a></li>
                <li><a href="http://blog.gastromama.com/gluten-free-restaurants-usa/">Eating Gluten Free in USA: How To Find The Best Restaurants</a></li>
                <li><a href="http://blog.gastromama.com/sushi-gluten/">Sushi and gluten: what to be careful about</a></li>
                <li><a href="http://blog.gastromama.com/gluten-contamination-italy/">Gluten: what to care about during your holidays in Italy</a></li>
        </ul>');

define("PRIVACY","Privacy policy");
define("SERVICE","Term of service");
define("COPYRIGHT","Gastromama Ltd © 2015");
define("COMPANY","");
define("TWITTER_URL","http://twitter.com/gastromama_en");

define("COOKIE_LAW",'To guarantee an optimized navigation and content experience, Gastromama uses cookies, 
    even of third parties. By continuing to use our site or by closing this window you agree to the processing 
    of your personal data as described in our 
    <a style="text-decoration: underline" href="/page/privacy/">Privacy Policy</a>. 
    <a href="#" class="cookie-agree"><span style="position: absolute;right: 10px;top: 10px;" class="glyphicon glyphicon-remove-sign glyphicon-close" aria-hidden="true"></span></a>');
define("MOTTO","Discover gluten free locations in a snap.");
