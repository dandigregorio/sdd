<?php

/*
------------------
Language: Italiano
------------------
*/
 
// NAVBAR
define("HOW", "Come funziona");
define("CONTACT", "Contatto");
define("LOGIN", "Accedi");
define("CITYPICKER", "Inserisci una città");
define("ADD_PLACE", "Piazza un nuovo radar");
define("ADD_PLACE", "Place another radar");
define("QUIT", "Esci");

// LOGIN/REGISTER FORM
define("FB_LOGIN","Accedi con Facebook");
define("FB_REGISTER","Registrati con Facebook");

define("ALTERNATIVE","oppure");
define("OR_MAIL","oppure via email");

define("LOGIN","Accedi");
define("REGISTER","Registrati");

define("NOT_REGISTER","Non sei registrato?");
define("REGISTER_NOW","Registrati!");

define("REGISTERED","Già registrato?");
define("LOGIN_NOW","Accedi!");

define("INSERT_EMAIL","Inserisci la tua email");
define("CHOOSE_PSW","Scegli una password");
define("VERIFY_PSW","Conferma la password");



// FOOTER
define("PAGES","Pagine");
define("BLOG_POST","Dal blog");
define("BLOG_LIST",'<ul class="footer-links">
                <li><a href="http://blog.gastromama.it/senza-glutine-mercato-testaccio/">Senza glutine al mercato di Testaccio</a></li>
                <li><a href="http://blog.gastromama.it/la-puntata-di-report-sul-mercato-degli-alimenti-senza-glutine-in-italia/">La puntata di Report sul mercato degli alimenti senza glutine in Italia</a></li>
                <li><a href="http://blog.gastromama.it/dove-eravamo-rimasti/">Dove eravamo rimasti?</a></li>
        </ul>');

define("PRIVACY","Politica di riservatezza");
define("SERVICE","Termini di servizio");
define("COPYRIGHT","Gastromama © 2015");
define("COMPANY","P.Iva 09983791006");
define("TWITTER_URL","http://twitter.com/gastromama_it");



define("COOKIE_LAW",'Per garantirti una esperienza di navigazione e di contenuti ottimizzata Gastromama utilizza le cookies, 
    anche di terze parti. Continuando a navigare nel nostro sito o chiudendo questra finestra acconsenti al trattamento dei tuoi dati come descritto
    nella nostra <a style="text-decoration: underline" href="/page/privacy/">Privacy Policy</a>. 
    <a href="#" class="cookie-agree"><span style="position: absolute;right: 10px;top: 10px;" class="glyphicon glyphicon-remove-sign glyphicon-close" aria-hidden="true"></span></a>');
define("MOTTO","Scopri locali gluten free ovunque, in un attimo.");