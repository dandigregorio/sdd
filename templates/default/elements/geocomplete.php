<?php include_once("/templates/default/lang/it.php") ?>

<style>
    .pac-container {
    z-index: 1051 !important;
}
    .map_canvas {
        position:relative;
        width:100%;
        height:300px;
    }
</style>

<div class="alert alert-warning alert-dismissible" role="alert">
    
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
        </button>
        <?php echo ADDPLACE_ALERT; ?>
</div>

<form id='addplace_frm'>

    <div>
        <input id="geocomplete" class="form-control ui-widget" type="text" placeholder="<?php echo LOGIN; ?>" />
        <span class="input-group-btn">
            <button style="visibility: hidden;" id="find" type="button"  value="find" class="btn btn-warning btn pull-right btn-group"> 
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>  
    </div>

      
      <input name="location" type="hidden" value="">
      <input name="route" type="hidden" value="">
      <input name="postal_code" type="hidden" value="">
      <input name="locality" type="hidden" value="">
      <input name="country" type="hidden" value="">
      <input name="country_short" type="hidden" value="">
      <input style="visibility: hidden;" id="find" type="button" value="find" />
</form>
        
    
    <div class="map_canvas"></div>
    
<!--    <pre id="logger">Log:</pre>-->
    
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?callback=initialize&libraries=places&sensor=false"></script>
    <script type="text/javascript" src="/js/plugin/geocomplete/jquery.geocomplete.min.js"></script>
<!--    <script src="/js/plugin/geocomplete/examples/logger.js"></script>-->
     
    <script>
      function initialize(){
        
        var options = {
          map: ".map_canvas",
          markerOptions: {draggable: true},
          details: "#addplace_frm",
          types: ['address']
        };
        
        $("#geocomplete").geocomplete(options)
          .bind("geocode:result", function(event, result){
              
//                $.log("Result: " + result.formatted_address);
                console.log(result);

          })
          .bind("geocode:error", function(event, status){
//            $.log("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
//            $.log("Multiple: " + results.length + " results found");
          });
        
        $("#find").click(function(){
          $("#geocomplete").trigger("geocode");
        });
        
        $("#examples a").click(function(){
          $("#geocomplete").val($(this).text()).trigger("geocode");
          return false;
        });
        
      }
    </script>