// Templates::default

//        $('#foodType').on('click','.fType', (function() {
//        var city = $("#city").val()
//        var query = $(this).attr("rel");
//        var econdedUrl=encodeURI('/app.php?lang=' + varGet["lang"]+ '&page=' + varGet["page"]+ '&query=' + query + '&location=' + city);
//        window.location=econdedUrl; 
//        //if ($(this).val() === '2') {
//        // Do something for option "b"
//        //alert(query);
//        //}
//        }));

// CITY AUTOCOMPLETE SECTION
// http://www.jqueryautocomplete.com/jquery-autocomplete-json-example.html
// http://api.jqueryui.com/autocomplete/           


$( document ).ready(function() {
    
   
    
    
$(function() {
    if (varSess['user_id']) {populateRadar();}
    var cache = {};   
    $( "#city" ).autocomplete({
         minLength: 1,
         source: function( request, response ) {
                        console.log(request);
                        var term = request.term;
                        if ( term in cache ) {
                                response( cache[ term ] );
                                return;
                        }
                        $.getJSON( "/APIs/geonames.php?lang="+varGet["lang"], request, function( data, status, xhr ) {
                            cache[ term ] = data;
                            response( data );
                        });
                },
         focus:  function( event, response ) {
             $("#city").attr("latlng","");
         },      
         change:  function( event, response ) {
             $("#city").attr("latlng","");
         }, 
         response:  function( event, response ) {
             $("#city").attr("latlng","");
         }, 
         select: function( event, response ) {
                console.log("scelto!")
                //$( "#city" ).val( response.item.label + " / " + response.item.latlng );
                $( "#city" ).val( response.item.label);
                $("#city").attr("latlng",response.item.latlng);
                //var econdedUrl=encodeURI('/app.php?lang=' + varGet["lang"]+ '&page=scan&query=' + varGet["query"] + '&location=' + $("#city").val() + '&latlng=' + $( "#city" ).attr('latlng'));
                if (varGet["lang"] == "en" || varGet["lang"] == "it" ) {langdir="";} else {langdir="/" + varGet["lang"];}
                var econdedUrl=encodeURI(langdir + '/glutenfree/' + $("#city").val() + '/' + $( "#city" ).attr('latlng') + '/');
                window.location=econdedUrl;  
                return true;
        }

    })
 
    if ($.cookie("privacylaw")) {
        console.log("Privacy law già impostata");
        console.log($.cookie);
    } else {
        $.cookie("privacylaw", true);
        $("#privacyLaw").removeClass("hidden")
        console.log($.cookie);
    }
       
    
    $('.cookie-agree').click(function(){
        $.cookie("privacylaw", true);
        $("#privacyLaw").addClass("hidden");
        return false;
    });



});

$('#city').click(function(){
    $('#city').val("");
    $("#city").attr("latlng","");
    $('#btnSniff').prop('disabled', true)

});



$( "#btnSniff" ).click(function() {
     var city = $("#city").val()
     var latlng = $( "#city" ).attr('latlng');
    if (varGet["lang"] == "en" || varGet["lang"] == "it" ) {langdir="";} else {langdir="/" + varGet["lang"];}
    var econdedUrl=encodeURI(langdir + '/glutenfree/' + $("#city").val() + '/' + $( "#city" ).attr('latlng') + '/');
     window.location=econdedUrl;        
});
    

$(function () {
  $('[data-toggle="tooltip"]').tooltip({
      show: { effect: "blind", duration: 800 },
      hide: { effect: "explode", duration: 1000 }
  });
})

$( ".addplace" ).click(function() {
            BootstrapDialog.show({
            title: themeTXT.ADDPLACE_TITLE,
            message: $('<div></div>').load('/templates/default/elements/geocomplete.php')
        });
        
});
    
 function populateRadar() {
    url='/backend/listRadar.php';
    $.getJSON( url, function( jsonresp ) {
        $(".usermenu").prepend("<li class=\"divider\"></li>");
        console.log("Voci menu >" + jsonresp.data.length);
        console.log(jsonresp);
        if (jsonresp.data.length > 0) {
            
            $.each(jsonresp.data, function(index,item) {
                if (varGet["lang"] == "en" || varGet["lang"] == "it" ) {langdir="";} else {langdir="/" + varGet["lang"];}
                $(".usermenu").prepend('<li id="item_' + item.placename + '"> <a href="' + langdir +'/glutenfree/radar/' + item.user_id +'/' + item.placename + '/" class=""><span class="glyphicon glyphicon-record"></span> ' + item.placename + '</a></li>');
            });
            
        } else {
               $(".usermenu").prepend('<li><a style="color:lightgrey">' + themeTXT.NORADAR_ITEM + '</a></li>');
        }
        $(".usermenu").prepend('<li class=\"dropdown-header\">' + themeTXT.YOUR_RADARS + '</li>');
    });
    
} 





//if ($.cookie("blogfeedx")) {
//        populatePostList();
//} else {
//        var data = {
//            q: 'http://feeds.feedburner.com/blog-gastromama-it'
//            , num: 5
//            , output: 'json'
//            , v: '1.0'
//        };
//        $.ajax({
//            url:'http://ajax.googleapis.com/ajax/services/feed/load'
//            ,type : "GET"
//            ,dataType : "jsonp"
//            ,data: data
//            ,success: function (json) {
//                var blogpost = new object();
//                feed = JSON.json.responseData.feed.entries;
//                $.each(feed, function(index,item) {
//                    
//                });
//                $.cookie("blogfeed", feed);
//                populatePostList($.cookie("blogfeed"));
//
//            }
//        });
//}
//
//function populatePostList(blogfeed) {
//     //posts = JSON.parse(blogfeed);
//     console.log(blogfeed);
// }



});