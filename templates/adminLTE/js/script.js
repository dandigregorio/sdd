$(document).ready(function () {
    
    // VISUALIZZAZIONE BLOCCHI PER RUOLO
    if (varSess['role']) {
        //Nasconde tutti gli strumenti. IN PRODUZIONE USARE remove();
        console.log("$('.role').hide()")
        $('.role').hide();
        //Abilita quelli del ruolo utente
        console.log("$('.role ." + varSess['role'] + "').show()")
        $('.role.' + varSess['role']).show();
    }
    
});