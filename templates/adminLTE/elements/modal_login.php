
<!-- Modal Login window-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo LOGIN; ?></h4>
      </div>

      <div class="modal-body" >

          
<div id="tabLogin">
    <p class="text-center"><button type="button" class="btn btn-primary btn-lg" id="fbLogin"><?php echo FB_LOGIN; ?></button></p>
    <p id="status"></p>
    <p class="text-muted text-center" style="margin:45px auto 25px auto;"><?php echo OR_MAIL; ?></p>
        
    <div class="container" style="margin: 0 auto;width: 70%;">
        <form id="frmLogin" class="form-horizontal">
        <fieldset>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Email" name="email" id="username" />
                </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password"  name="password" id="password" />
            </div>
            <div id="msgLogin" class="form-group"></div>
            <div class="form-group">
                <p class="text-center"><button id="loginButton" type="submit" class="btn btn-default btn-sm"><?php echo LOGIN; ?></button></p>
            </div>
        </fieldset> 
        </form>
    </div>    
</div>    

          
          
<div id="tabRegister">
    <p class="text-center"><button type="button" class="btn btn-primary btn-lg" id="fbLogin"><?php echo FB_REGISTER; ?></button></p>
    <p class="text-muted text-center" style="margin:45px auto 25px auto;"><?php echo ALTERNATIVE; ?></p>
        
    <div class="container" style="margin: 0 auto;width: 70%;">
        <form id="frmRegister" class="form-horizontal">
            <fieldset>
                
            <!-- Emal input-->
            <div class="form-group"><div class="input-group">
                <span class="input-group-addon">@</span>
                <input id="email" name="email" class="form-control" placeholder="<?php echo INSERT_EMAIL; ?>" type="text" required="">
            </div></div>

            <!-- Password input-->
            <div class="form-group">
                <input id="password" name="password" type="password" class="form-control" placeholder="<?php echo CHOOSE_PSW; ?>"  required="">
            </div>

            <!-- Verify password input -->
            <div class="form-group">
                <input id="vpassword" name="vpassword"  type="password" class="form-control" placeholder="<?php echo VERIFY_PSW; ?>" >
            </div>
            
            <div id="msgRegister" class="form-group"></div>
            <!-- Submit Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="btnRegister"></label>
                <button type="submit" id="btnRegister" name="btnRegister" class="btn btn-default"><?php echo REGISTER; ?></button>
            </div>
            </fieldset>
        </form>
    </div>    
</div>
          
</div>
    <div class="modal-footer" id="footer">
        <small><span id="ftrLabel"><?php echo NOT_REGISTER; ?></span> <a id="lnkToggle" status="login" href="#"><?php echo REGISTER_NOW; ?></a></small>
    </div> 
      </div>
    </div>
  </div>
<script>

/////////////////////////////////////////////////
// HIDE REGISTRATION FORM AT MODAL OPENING    //
/////////////////////////////////////////////////

function setLogin() {
    $('#tabRegister').hide();
    $('#frmLogin').data('bootstrapValidator').resetForm(true);
    $('#tabLogin').show();  
    $('#lnkToggle').attr('status','login');
    $('#lnkToggle').text(themeTXT.REGISTER_NOW);
    $('#myModalLabel').text(themeTXT.LOGIN_NOW);
    $('#ftrLabel').text(themeTXT.NOT_REGISTER);    
}

function setRegister() {
    $('#tabLogin').hide();  
    $('#frmRegister').data('bootstrapValidator').resetForm(true);
    $('#tabRegister').show();
    $('#lnkToggle').attr('status','register');
    $('#lnkToggle').text(themeTXT.LOGIN_NOW);
    $('#myModalLabel').text(themeTXT.REGISTER_NOW);
    $('#ftrLabel').text(themeTXT.REGISTERED);  
}


    $('#myModal').on('show.bs.modal', function() {
        $('#loginForm').bootstrapValidator('resetForm', true);
        setLogin();          
    });
    
    $('#myModal').on('hidden.bs.modal', function() {
         $('#frmRegister').bootstrapValidator('resetForm', true);
         $('#frmLogin').bootstrapValidator('resetForm', true);
         $('#myModal .alert').remove();
         $("#fbLogin").html("Accedi con Facebook");
        //var $invoker = $(e.relatedTarget.attr("href"));
        //console.log($(".getAuth").attr('href'));
        //console.log($invoker);
})

/////////////////////////////////////////////////
// TOGGLE LOGIN/REGISTER TABLE ON FOOTER CLICK //
/////////////////////////////////////////////////
$('#lnkToggle').click(function() {          
        
        if (($('#lnkToggle').attr('status')) === 'login') {
            setRegister();
        } else {
            setLogin();
        }
});

    

/////////////////////////////////////////////////
// REGISTER FORM                               //
/////////////////////////////////////////////////

    

// VALIDATE THE REGISTER FORM ///////////////////
 
$('#frmRegister').bootstrapValidator({
    excluded: ':disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        email: {
            validators: {
                notEmpty: {
                    message: themeTXT.INSMAIL
                },
                emailAddress : {
                    message: themeTXT.VALIDMAIL
                }
                    
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: themeTXT.PICKPASS
                },
                stringLength: {
                    min: 6,
                    message: themeTXT.LONGERPASS
                }      
            }
        },
        vpassword: {
            validators: {
                notEmpty: {
                    message: themeTXT.CONFPASS
                },
                identical : {
                    field: 'password',
                    message: themeTXT.DIFFPASS
                }
            }
        }
    }
});



// HANDLE THE REGISTER FORM SUBMIT //////////////////////

$( "#frmRegister" ).on('success.form.bv', function(e) {
    // Prevent form submission
    e.preventDefault();
    
    var usrEmail = $("#frmRegister input[name$='email']").val();
    var usrPass = $("#frmRegister input[name$='password']").val();
    var usrUsername = usrEmail.split("@")[0];
    //alert(usremail + usrpass + username);
    $.post('/callController.php', {
                controller: "users",
                action: "register",
                email: usrEmail, 
                password: usrPass,
                username: usrUsername
            }, function(data) {
                console.log(data);
               if (data == 1) {    
                            //alert('OK: '  + data + 'x');
                            $("#tabRegister").html('<div class="alert alert-success" role="alert"><strong>'+ themeTXT.ALMOST_DONE +'</strong></div>\n\
                            <p>'+ themeTXT.WAIT_CONFIRM +'</p>');
                            $("#footer").empty();
                } else {
                        //alert('ERROR: ' + data + 'x');
                        $("#msgRegister").html('<div class="alert alert-danger" role="alert">'+ themeTXT.USER_EXISTS +'</div>');
                        return false;
                }
   });

});



/////////////////////////////////////////////////
// LOGIN FORM                                  //
/////////////////////////////////////////////////


// Validate the user input //////////////////////

$('#frmLogin').bootstrapValidator({
    excluded: ':disabled',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        email: {
            validators: {
                notEmpty: {
                    message: themeTXT.INSMAIL
                },
                emailAddress : {
                    message: themeTXT.VALIDMAIL
                }
                    
            }
        },
        password: {
            validators: {
                notEmpty: {
                    message: themeTXT.PICKPASS
                },
                stringLength: {
                    min: 6,
                    message: themeTXT.LONGERPASS
                }      
            }
        }

    }
});

// Handle login submit //////////////////////

$( "#frmLogin" ).on('success.form.bv', function(e) {
    // Prevent form submission
    e.preventDefault();
    
    var usremail = $("#frmLogin input[name$='email']").val();
    var usrpass = $("#frmLogin input[name$='password']").val();
    //alert( usremail + usrpass );
    $.post('/callController.php', {
                controller: "users",
                action: "auth",
                email: usremail, 
                password: usrpass,
                userCity: geoplugin_city(),
                userCountryCode: geoplugin_countryCode(),
                userLat: geoplugin_latitude(),
                userLng: geoplugin_longitude(),
                userIp:  geoplugin_request()
            }, function(data) {
               if (data == 1) {    
                            //alert('OK: '  + data + 'x');
                            $('.modal.in').modal('hide');
                            window.location.reload(true); 
                } else {
                        //alert('ERROR: ' + data + 'x');
                        $("#msgLogin").html('<div class="alert alert-danger" role="alert">' + themeTXT.AUTH_FAIL + '</div>');
                        return false;
                }
event.preventDefault();
});
    
    
    
});

/////////////////////////////////////////////////
// FACEBOOK LOGIN HANDLER                      //
/////////////////////////////////////////////////
//$("#fbLogin").click(function() {
//    //alert(window.location.href);
//    //window.location = "http://www.google.com/"
//    
//    var redirect = window.location.href;
//    // window.location = "controllers/facebook/fbauth.php";
//    $('.modal.in').modal('hide');
//    alert("controllers/facebook/fbauth.php?redirect=" + redirect);
//    window.location = "controllers/facebook/fbauth.php?redirect=" + redirect;
//});

$("#fbLogin").click(function() {
   $("#fbLogin").html("<i class=\"fa fa-spinner fa-spin\"></i> Connessione FB in corso"); 
   function statusChangeCallback(response) {
       var fbAvatar;
        //console.log('statusChangeCallback');
        
        if (response.status === 'connected') {
                // Logged into your app and Facebook.

                //console.log("connected");
                //console.log(response);
                FB.api(
                        "/me/picture?redirect=0&height=80&type=normal&width=80",
                        function (pic) {
                            if (pic && !pic.error) {fbAvatar = pic.data.url;}
                        }
                    );

                FB.api('/me', function(fbuser) {     

                    $.post('/callController.php', {
                        controller: "users",
                        action: "FBauth",
                        email: fbuser.email,
                        fbId:fbuser.id,     
                        fbUsername:fbuser.first_name,
                        firstname:fbuser.first_name,
                        lastname:fbuser.last_name,
                        locale:fbuser.locale,
                        gender:fbuser.gender,
                        fbAvatar: fbAvatar,
                        userCity: geoplugin_city(),
                        userCountryCode: geoplugin_countryCode(),
                        userLat: geoplugin_latitude(),
                        userLng: geoplugin_longitude(),
                        userIp:  geoplugin_request()
                    }, function(data) {
                        //console.log("data");
                        //console.log(data);
                        if (data.ok) {    

                                  $('.modal.in').modal('hide');
                                  window.location.reload(true); 
                        } else {                       
                                $("#fbLogin").html("Connessione FB non riuscita");
                        }
                    });
                });
        } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.

    } else {
        
            $('.modal.in').modal('hide');  
            FB.login(function(response) {
                    if (response.status === 'connected') {
                            statusChangeCallback(response);
                    } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                     } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                    }
            },{scope: 'email'});
    }
    
  }
  // 
  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

// This code will load and initialize the SDK. Replace the value in your-app-id with the ID 
// of your own Facebook App. You can find this ID using the App Dashboard.
// MORE INFO ON: https://developers.facebook.com/docs/facebook-login/login-flow-for-web/v2.1

  window.fbAsyncInit = function() {
        FB.init({
            appId      : '502483869802149',
            cookie     : true,  // enable cookies to allow the server to access 
                        // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.2' // use version 2.1
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

 };
 
    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/it_IT/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


});
</script>    