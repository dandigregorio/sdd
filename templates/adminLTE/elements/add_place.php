<style>
    .pac-container {
    z-index: 1051 !important;
}

</style>
<script src="http://maps.googleapis.com/maps/api/js?callback=initialize&amp;sensor=false&amp;libraries=places" type="text/javascript"></script>
<script type="text/javascript">
    
function initialize() {

 var options = {
  types: ['address'],
  //componentRestrictions: {country: "us"}
 };

 var input = document.getElementById('searchTextField');
 var autocomplete = new google.maps.places.Autocomplete(input, options);
}
   //google.maps.event.addDomListener(window, 'load', initialize);
   
</script>



<form role="form">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <input id="searchTextField" class="form-control" type="text" size="50" placeholder="Enter a location" autocomplete="on">
            </div>
        </div>
    </div>
</form>

<form role="form" id="contact-form" class="contact-form bv-form" novalidate="novalidate"><button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                    <div class="row">
                		<div class="col-md-6">
                  		<div class="form-group has-feedback">
                            <input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="Name" data-bv-field="Name"><i class="form-control-feedback bv-no-label" data-bv-icon-for="Name" style="display: none;"></i>
                  		<small class="help-block" data-bv-validator="notEmpty" data-bv-for="Name" data-bv-result="NOT_VALIDATED" style="display: none;">The Name is required and cannot be empty</small></div>
                  	</div>
                    	<div class="col-md-6">
                  		<div class="form-group has-feedback">
                            <input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="E-mail" data-bv-field="email"><i class="form-control-feedback bv-no-label" data-bv-icon-for="email" style="display: none;"></i>
                  		<small class="help-block" data-bv-validator="notEmpty" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The email address is required</small><small class="help-block" data-bv-validator="emailAddress" data-bv-for="email" data-bv-result="NOT_VALIDATED" style="display: none;">The email address is not valid</small></div>
                  	</div>
                  	</div>
                  	<div class="row">
                  		<div class="col-md-12">
                  		<div class="form-group has-feedback">
                            <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message" data-bv-field="Message"></textarea><i class="form-control-feedback bv-no-label" data-bv-icon-for="Message" style="display: none;"></i>
                  		<small class="help-block" data-bv-validator="notEmpty" data-bv-for="Message" data-bv-result="NOT_VALIDATED" style="display: none;">The Message is required and cannot be empty</small></div>
                  	</div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                  <button type="submit" class="btn main-btn pull-right">Send a message</button>
                  </div>
                  </div>
                </form>