<?php

/*------------------
Language: Italiano
------------------*/

// EMAIL
$email['sender_name'] = "Gastromama";
$email['sender_email'] = "noreply@gastromama.it";
$email['verification_subject'] = "[Gastromama] - Please confirm your subscription";
$email['verification_body'] = "<p>Hello!</p> 
            <p>It's a pleasure to have you on board. Please confirm your subscription clicking on the link below:<br/>
            %s</p>
            <p>Thanks an we hope to see you soon!</p> 
            <p>Gastromama staff</p>";