<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a href="/" class="navbar-brand"><b>SDD</b>NEW</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- NAVBAR SINISTRA-->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- ADMIN-->
                    <li class="dropdown role admin">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Admin <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/page/admin/users/">Utenti</a></li>

                        </ul>
                    </li>
                    <!-- DATI-->
                    <li class="dropdown role admin viewer">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Dati <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/page/dati/strumenti/">Strumenti</a></li>

                        </ul>
                    </li>

                </ul>
                <form class="navbar-form navbar-left" role="search" style="display:none">
                    <div class="form-group">
                        <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                    </div>
                </form>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->


                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="/img/buddy.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?php echo $_SESSION['fname'] . " " . $_SESSION['sname'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/img/buddy.png" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo $_SESSION['fname'] . " " . $_SESSION['sname'] ?>
                                    <small><?php echo $_SESSION['user'] ?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat" disabled>Impostazioni</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/logout.php" class="btn btn-default btn-flat">Esci</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>