<?php


//// FOURSQUARE API 
$query=  urlencode($_GET['query']);
$city=urlencode($_GET['location']);
if ($_GET['radius']) {$radius="&radius=".urlencode($_GET['radius']);} else {$radius="";};
$FSquare_CID="ICPYCI4ZADOFBA4O5L5ZLDSNGAYPULH0MZNG3HAYVKBJQUYD";
$FSquare_CS="0MED2HTQ033MR52QIHJ3NSXRK1DPXVEYJJWYAWTT3DJQEABU";
//$fqApiUrl="https://api.foursquare.com/v2/venues/explore?query=".$query."&near=".$city."&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20141016&limit=100";
$fqApiUrl="https://api.foursquare.com/v2/venues/explore?query=".$query."&ll=51.5099931,-0.1351718&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20141016&limit=50".$radius;
$data  = json_decode(file_get_contents($fqApiUrl));

$myItems=$data->response->groups[0]->items;


/////////////////////// PER OGNI FOURSQUARE  VENUE
foreach ($myItems as $item) {
        $tipCount=0;
        $tipCount=count($item->tips);
        unset($tipText);
        $telCode=str_replace(' ', '',preg_replace("/^[A-Z]+$/i", "", strtolower($item->venue->name)));
        
        /////////////////////// PER OGNI RECENSIONE
        foreach ($item->tips as $tip) {   

                     $tips[$telCode][]= array (
                            'tipFrom' => "Foursquare",
                            'tipText' => $tip->text,
                            'tipUser' => $tip->user->firstName,
                            'tipUserpic' => $tip->user->photo->prefix."25x25".$item->tips[0]->user->photo->suffix,
                            'tipUrl' => $tip->canonicalUrl,
                            'tipImg' => 'https://ss1.4sqi.net/img/foursquare_32-1552bc2b5cdd7fb9356541b2fecc0add.png' 
                             );    
                } //////// FINE PER OGNI RECENSIONE
                
                
        /////////////////////// PER OGNI FOTO
        foreach ($item->venue->photos->groups[0]->items as $pic) {
                    $pics[$telCode][]= array (
                        'picFrom' => "Foursquare",
                        'picUrl' => $pic->prefix."300x300".$pic->suffix
                        );
        } //////////////// FINE PER OGNI FOTO
        
        
                
        if ($listed[$telCode]==false) {    
                                    $Venues[$telCode]['belongTo']="Foursquare";
                                    $Venues[$telCode]['name']=$item->venue->name;
                                    $Venues[$telCode]['lat']=$item->venue->location->lat;
                                    $Venues[$telCode]['lng']=$item->venue->location->lng;
                                    $Venues[$telCode]['distance']=$item->venue->location->distance;
                                    $Venues[$telCode]['address']=$item->venue->location->address;
                                    $Venues[$telCode]['zip']=$item->venue->location->postalCode;
                                    $Venues[$telCode]['city']=$item->venue->location->city;
                                    $Venues[$telCode]['state']=$item->venue->location->state;
                                    $Venues[$telCode]['country']=$item->venue->location->country;
                                    $Venues[$telCode]['phone']=$item->venue->contact->formattedPhone;
                                    $Venues[$telCode]['category']=$item->venue->categories->name;
                                    $Venues[$telCode]['url']=$item->venue->url;
                                    $Venues[$telCode]['photo']=$item->venue->photos->groups[0]->items[0]->prefix."300x300".$item->venue->photos->groups[0]->items[0]->suffix;
                                    //$Venues[$telCode]['tips']=array();

            }       
            $listed[$telCode]==true;
            
        } /////////////////////// FINE PER OGNI FOURSQUARE VENUE

foreach ($Venues as $key => $value) {
                   
                    $myJson[]= array(   
                                    'belongTo' => $Venues[$key]['belongTo'],
                                    'name' => $Venues[$key]['name'],
                                    'lat' => $Venues[$key]['lat'],
                                    'lng' => $Venues[$key]['lng'],
                                    'distance' => $Venues[$key]['distance'],
                                    'address' => $Venues[$key]['address'],
                                    'zip' => $Venues[$key]['zip'],
                                    'city' => $Venues[$key]['city'],
                                    'state' => $Venues[$key]['state'],
                                    'country' => $Venues[$key]['country'],
                                    'phone' => $Venues[$key]['phone'],
                                    'category' => $Venues[$key]['category'],
                                    'url' => $Venues[$key]['url'],
                                    'photos' => $Venues[$key]['photo'],
                                    'tips' => $tips[$key],
                                    'pics' =>$pics[$key]
                                );
        

}
     
?>

<?php
header('Content-Type: application/json; charset=UTF8');
echo json_encode($myJson);
//print_r($myJson);
?>
