<?php
require_once('libs/yelp-oauth.php');

$CONSUMER_KEY = 'WU26ezEce9xX5gxNVgx5xQ';
$CONSUMER_SECRET = 'iTqGXSSScnmRfyVOEMM2ayqKBCc';
$TOKEN = '3TetnGiCEgXPWEqGjd1myDczFbfLId1D';
$TOKEN_SECRET = 'Wwj6jUXJp4urDozSssg7uCNjfJY';


$API_HOST = 'api.yelp.com';
$DEFAULT_TERM = $_GET['query'];
$DEFAULT_LOCATION = $_GET['location'];
//$DEFAULT_CATEGORY = $_GET['category'];
$DEFAULT_OFFSET = $_GET['offset'];
//$SEARCH_LIMIT = 3;
$SEARCH_PATH = '/v2/search/';
$BUSINESS_PATH = '/v2/business/';


/** 
 * Makes a request to the Yelp API and returns the response
 * 
 * @param    $host    The domain host of the API 
 * @param    $path    The path of the APi after the domain
 * @return   The JSON response from the request      
 */
function request($host, $path) {
    $unsigned_url = "http://" . $host . $path;

    // Token object built using the OAuth library
    $token = new OAuthToken($GLOBALS['TOKEN'], $GLOBALS['TOKEN_SECRET']);

    // Consumer object built using the OAuth library
    $consumer = new OAuthConsumer($GLOBALS['CONSUMER_KEY'], $GLOBALS['CONSUMER_SECRET']);

    // Yelp uses HMAC SHA1 encoding
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

    
    $oauthrequest = OAuthRequest::from_consumer_and_token(
        $consumer, 
        $token, 
        'GET', 
        $unsigned_url
    );
    
    // Sign the request
    $oauthrequest->sign_request($signature_method, $consumer, $token);

    // Get the signed URL
    $signed_url = $oauthrequest->to_url();
    //echo "$signed_url";
    // Send Yelp API Call
    $ch = curl_init($signed_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    
    return $data;
}

/**
 * Query the Search API by a search term and location 
 * 
 * @param    $term        The search term passed to the API 
 * @param    $location    The search location passed to the API 
 * @return   The JSON response from the request 
 */
function search($term, $location) {
    $url_params = array();
    
    $url_params['term'] = $term ?: $GLOBALS['DEFAULT_TERM'];
    $url_params['location'] = $location?: $GLOBALS['DEFAULT_LOCATION'];
    $url_params['limit'] = $GLOBALS['SEARCH_LIMIT'];
    //$url_params['category_filter'] = $GLOBALS['DEFAULT_CATEGORY'];
    $url_params['offset'] = $GLOBALS['DEFAULT_OFFSET'];
    $search_path = $GLOBALS['SEARCH_PATH'] . "?" . http_build_query($url_params);
    
    return request($GLOBALS['API_HOST'], $search_path);
}

/**
 * Query the Business API by business_id
 * 
 * @param    $business_id    The ID of the business to query
 * @return   The JSON response from the request 
 */
function get_business($business_id) {
    $business_path = $GLOBALS['BUSINESS_PATH'] . $business_id;
    
    return request($GLOBALS['API_HOST'], $business_path);
}

/**
 * Queries the API by the input values from the user 
 * 
 * @param    $term        The search term to query
 * @param    $location    The location of the business to query
 */
function query_api($term, $location) {     
    $response = json_decode(search($term, $location));
//    $business_id = $response->businesses[0]->id;
    
//    print sprintf(
//        "%d businesses found, querying business info for the top result \"%s\"\n\n",         
//        count($response->businesses),
//        $business_id
//    );
    
//    $response = get_business($business_id);
    
//    print sprintf("Result for business \"%s\" found:\n", $business_id);
//    print "$response\n";
    
    return $response;
}



/**
 * User input is handled here 
 */
$longopts  = array(
    "term::",
    "location::",
    //"category_filter::",
    "offset::",
);
    
$options = getopt("", $longopts);

$term = $options['term'] ?: '';
$location = $options['location'] ?: '';
//$category_filter = $options['category_filter'] ?: '';
$offset = $options['offset'] ?: '';

$data = array();
//$data=query_api($term, $location,$offset);

foreach ($data->businesses as $business) {
        unset($tipText);
        $telCode=str_replace( ' ', '',preg_replace("/^[A-Z]+$/i", "", strtolower($business->name)));

        
        if (strpos($business->snippet_text,'gluten')) {
                $tipText=$business->snippet_text;
            } elseif(in_array('Gluten-Free', array($business->categories[0][0],$business->categories[1][0],$business->categories[2][0]))) {
                $tipText="This place is listed in Gluten free category on Yelp";
            } else {
                $tipText="We've detected reviews of this place about gluten on Yelp";
            }
            
        $tips[$telCode][]= array (
                            'tipFrom' => "Yelp",
                            'tipText' => $tipText,
                            'tipUser' => "",
                            'tipPic' => ""
                        );    
            
         if ($listed[$telCode]==false) {   
                                    $Venues[$telCode]['belongTo']="Yelp";
                                    $Venues[$telCode]['name']=$business->name;
                                    $Venues[$telCode]['lat']=$business->location->coordinate->latitude;
                                    $Venues[$telCode]['lng']=$business->location->coordinate->longitude;
                                    $Venues[$telCode]['address']=$business->location->display_address[0];
                                    $Venues[$telCode]['zip']=$business->location->postal_code;
                                    $Venues[$telCode]['city']=$business->location->city;
                                    $Venues[$telCode]['state']=$business->location->display_address[3];
                                    $Venues[$telCode]['country']=$business->location->country_code;
                                    $Venues[$telCode]['phone']=$business->display_phone;
                                    $Venues[$telCode]['category']=implode(",", $business->categories);
                                    $Venues[$telCode]['url']=$business->url;
                                    $Venues[$telCode]['photo']="";
                                    //$Venues[$telCode]['tips']=array();

         }
         
         $listed[$telCode]==true;
    
    }
    



//// FOURSQUARE API 

$username='';

$query=$_GET['query'];
$city=$_GET['location'];
$FSquare_CID="ICPYCI4ZADOFBA4O5L5ZLDSNGAYPULH0MZNG3HAYVKBJQUYD";
$FSquare_CS="0MED2HTQ033MR52QIHJ3NSXRK1DPXVEYJJWYAWTT3DJQEABU";
$fqApiUrl="https://api.foursquare.com/v2/venues/explore?query=".$query."&near=".$city."&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20141016&limit=100";

$data  = json_decode(file_get_contents($fqApiUrl));

$myItems=$data->response->groups[0]->items;

foreach ($myItems as $item) {
        unset($tipText);
        $telCode=str_replace(' ', '',preg_replace("/^[A-Z]+$/i", "", strtolower($item->venue->name)));
        foreach ($item->tips as $tip) {
            
                     $tips[$telCode][]= array (
                            'tipFrom' => "Foursquare",
                            'tipText' => $tip->text,
                            'tipUser' => $tip->user->firstName,
                            'tipPic' => $tip->user->photo->prefix."_x_".$item->tips[0]->user->photo->suffix
                        );    
                }
        
        if ($listed[$telCode]==false) {    
                                    $Venues[$telCode]['belongTo']="Foursquare";
                                    $Venues[$telCode]['name']=$item->venue->name;
                                    $Venues[$telCode]['lat']=$item->venue->location->lat;
                                    $Venues[$telCode]['lng']=$item->venue->location->lng;
                                    $Venues[$telCode]['address']=$item->venue->location->address;
                                    $Venues[$telCode]['zip']=$item->venue->location->postalCode;
                                    $Venues[$telCode]['city']=$item->venue->location->city;
                                    $Venues[$telCode]['state']=$item->venue->location->state;
                                    $Venues[$telCode]['country']=$item->venue->location->country;
                                    $Venues[$telCode]['phone']=$item->venue->contact->formattedPhone;
                                    $Venues[$telCode]['category']=$item->venue->categories->name;
                                    $Venues[$telCode]['url']=$item->venue->url;
                                    $Venues[$telCode]['photo']="";
                                    //$Venues[$telCode]['tips']=array();

            }       
            $listed[$telCode]==true;
        }

foreach ($Venues as $key => $value) {
    
                    $myJson[]= array(   
                                    'belongTo' => $Venues[$key]['belongTo'],
                                    'name' => $Venues[$key]['name'],
                                    'lat' => $Venues[$key]['lat'],
                                    'lng' => $Venues[$key]['lng'],
                                    'address' => $Venues[$key]['address'],
                                    'zip' => $Venues[$key]['zip'],
                                    'city' => $Venues[$key]['city'],
                                    'state' => $Venues[$key]['state'],
                                    'country' => $Venues[$key]['country'],
                                    'phone' => $Venues[$key]['phone'],
                                    'category' => $Venues[$key]['category'],
                                    'url' => $Venues[$key]['url'],
                                    'photo' => "",
                                    'tips' => $tips[$key],
                                );
        

}
     
?>
<?php
//header('Content-Type: application/json; charset=UTF8');
//echo json_encode($tips);
print_r($myJson);
?>
