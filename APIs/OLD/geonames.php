<?php

/* THIS SCRIPT MAKE AN API CALL TO Geonames.org TO GET
 * A LIST OF GEOVALUES FOR AUTOCOMPLETE. MORE INFO ON
 * http://www.geonames.org/export/geonames-search.html
 */

//$loc[] = array('label' => 'Roma');
//$loc[] = array('label' => 'Milano');
//$loc[] = array('label' => 'Napoli');
//$loc[] = array('label' => 'Torino');
//http://ws.geonames.org/searchJSON?name_startsWith=Lond&cities=cities15000&maxRows=10&orderby=relevance&username=mzarkoff
//
//$request=$_REQUEST['term'];
$request=$_GET['term'];
$lang=$_GET['lang'];
$username='mzarkoff';
$jsonSrc="http://ws.geonames.org/searchJSON?name_startsWith=$request&cities=cities15000&maxRows=10&orderby=relevance&username=$username&lang=$lang&orderby=population";
$data  = json_decode(file_get_contents($jsonSrc));
//echo $jsonSrc;
$geonames=$data->geonames;


foreach ($geonames as $city) {
        
        $cities[]= array(
            'label' => ucfirst($city->name).", ".$city->countryName,
            'latlng' => $city->lat.",".$city->lng);
        
        //print "<p>".$city->name.", ".$city->countryName."</p>";
}    

//print_r($cities);
echo json_encode($cities);
//print_r($data );
?>