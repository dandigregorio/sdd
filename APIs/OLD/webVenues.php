<?php

//COMMENT THIS BLOCK TO DISABLE CACHING
if      ($_GET['type'] == "radar") {$cachePath = "cache/radar/".cacheFile($_GET['ll']).".json";}
else if ($_GET['type'] == "city" || $_GET['type'] == "mobile" ||  $_GET['type'] == "scan" )  {$cachePath = "cache/location/".cacheFile($_GET['ll']).".json";}
//END CACHING BLOCK

 if (file_exists($cachePath)) {
    $cached =  json_decode(file_get_contents($cachePath),true);
    $cached['meta']['src'] = $cachePath;
    header('Content-Type: application/json; charset=UTF8');
    echo json_encode($cached,JSON_PRETTY_PRINT);
    exit;
}
    
$city=urlencode($_GET['location']);
if ($_GET['ll']) {$ll=$_GET['ll'];} else {$ll="51.5099931,-0.1351718";}
if ( intval($_GET['radius']) > 0 ) {$radius=intval($_GET['radius']);} else {$radius="7000";};



// SET THE SEARCH CENTER
$geo = explode(",",$ll);
$myJson['meta']['src'] = "on-the-fly";
$myJson['meta']['center']['lat'] = $geo[0];
$myJson['meta']['center']['lng'] = $geo[1];


$FSquare_CID="ICPYCI4ZADOFBA4O5L5ZLDSNGAYPULH0MZNG3HAYVKBJQUYD";
$FSquare_CS="0MED2HTQ033MR52QIHJ3NSXRK1DPXVEYJJWYAWTT3DJQEABU";


if ($_GET['query'] == 'vegan') { 
    $fqApiUrl[1]="https://api.foursquare.com/v2/venues/explore?query=vegan&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
    $fqApiUrl[2]="https://api.foursquare.com/v2/venues/explore?query=vegano&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
}
else if ($_GET['query'] == 'vegetarian') {
    $fqApiUrl[1]="https://api.foursquare.com/v2/venues/explore?query=vegetarian&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
    $fqApiUrl[2]="https://api.foursquare.com/v2/venues/explore?query=vegetariano&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;

} else {
    $fqApiUrl[1]="https://api.foursquare.com/v2/venues/explore?query=glutine&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
    $fqApiUrl[2]="https://api.foursquare.com/v2/venues/explore?query=gluten&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
    $fqApiUrl[3]="https://api.foursquare.com/v2/venues/explore?query='wheat free'&ll=".$ll."&sortByDistance=1&venuePhotos=1&client_id=".$FSquare_CID."&client_secret=".$FSquare_CS."&v=20160920&limit=50&radius=".$radius;
    
    $ylpKey[1]="glutine";
    $ylpKey[2]="gluten";
    
    $myJson['meta']['fqurl'] = $fqApiUrl; 
}

/////////////////////// PER OGNI FEED
while (list($n, $jsource) = each($fqApiUrl)) {
        
    
$data  = json_decode(file_get_contents($jsource));
$myJson['meta']['fq'][] = $data; 

if (!$myJson['meta']['bounds']) {
    $myJson['meta']['bounds']=$data->response->suggestedBounds;
    $swlat=$data->response->suggestedBounds->sw->lat;
    $swlng=$data->response->suggestedBounds->sw->lng;
    $nelat=$data->response->suggestedBounds->ne->lat;
    $nelng=$data->response->suggestedBounds->ne->lng;    
    //$myJson['meta']['bounds']['compact'] = "$swlat,$swlng|$nelat,$nelng";
}

$myItems=$data->response->groups[0]->items;


/////////////////////// PER OGNI FOURSQUARE  VENUE
foreach ($myItems as $item) {

        //$tipCount=0;
        //$tipCount=count($item->tips);
        unset($tipText);
        $vKey=vKey($item->venue->name,$item->venue->location->lat,$item->venue->location->lng);
        /////////////////////// PER OGNI RECENSIONE

        foreach ($item->tips as $tip) {   
                if ($tipIn[$tip->id]==false) { //// verifica inserimento recensione    
                     $tips[$vKey]['Foursquare']['tips'][]= array (
                            'tipText' => $tip->text,
                            'tipUser' => $tip->user->firstName,
                            'tipUserpic' => $tip->user->photo->prefix."50x50".$item->tips[0]->user->photo->suffix,
                            'tipUrl' => $tip->canonicalUrl,
                            'tipImg' => 'https://ss1.4sqi.net/img/foursquare_32-1552bc2b5cdd7fb9356541b2fecc0add.png' 
                             );   
                     $tipIn[$tip->id]=true;
                     
                }     
        }         
        $tipCount = count($tips[$vKey]['Foursquare']['tips']);
        if ($tipCount == 1)  {
                $tips[$vKey]['Foursquare']['meta']['it']['match'] = "Un commento sul glutine su Foursquare";
                $tips[$vKey]['Foursquare']['meta']['en']['match'] = "A review on gluten in Foursquare";
        } elseif  ($tipCount > 1) {
                $tips[$vKey]['Foursquare']['meta']['it']['match'] = $tipCount." commenti sul glutine su Foursquare";
                $tips[$vKey]['Foursquare']['meta']['en']['match'] = $tipCount." reviews about gluten on Foursquare";
        } else {
                $tips[$vKey]['Foursquare']['meta']['it']['match'] = "Indicato come gluten free su Foursquare";
                $tips[$vKey]['Foursquare']['meta']['en']['match'] = "Listed as gluten free on Foursquare";
        }
        $tips[$vKey]['Foursquare']['meta']['url']= "http://foursquare.com/v/".$item->venue->id;
      
        
        //////// FINE PER OGNI RECENSIONE
                
                
        /////////////////////// PER OGNI FOTO
        foreach ($item->venue->photos->groups[0]->items as $pic) {
                if ($picIn[$pic->id]==false) { //// verifica inserimento recensione    
                    $pics[$vKey][]= array (
                        'picFrom' => "Foursquare",
                        'picUrl' => $pic->prefix."500x500".$pic->suffix
                        );
                    $picIn[$pic->id]=true;
                }

        } //////////////// FINE PER OGNI FOTO
       

        if ($listed[$vKey]==false) {    
                    $Venues[$vKey]['id']=$vKey;
                    $Venues[$vKey]['belongTo']="Foursquare";
                    $Venues[$vKey]['name']=$item->venue->name;
                    $Venues[$vKey]['lat']=$item->venue->location->lat;
                    $Venues[$vKey]['lng']=$item->venue->location->lng;
                    $Venues[$vKey]['distance']=$item->venue->location->distance;
                    $Venues[$vKey]['address']=$item->venue->location->address;
                    $Venues[$vKey]['zip']=$item->venue->location->postalCode;
                    $Venues[$vKey]['city']=$item->venue->location->city;
                    $Venues[$vKey]['state']=$item->venue->location->state;
                    $Venues[$vKey]['country']=$item->venue->location->country;
                    $Venues[$vKey]['phone']=$item->venue->contact->formattedPhone;
                    $Venues[$vKey]['category']=$item->venue->categories->name;
                    $Venues[$vKey]['url']=$item->venue->url;
                    if ($item->venue->photos->groups[0]->items[0]->prefix) {
                        $Venues[$vKey]['photo']=$item->venue->photos->groups[0]->items[0]->prefix."500x500".$item->venue->photos->groups[0]->items[0]->suffix;

                    }
                    //$Venues[$vKey]['tips']=array();

            }       
            $listed[$vKey]=true;
            
        } 
/////////////////////// FINE PER OGNI FOURSQUARE VENUE

 }      

/////////////////////// FINE PER OGNI FEED    

 
//////////////////// YELP

 require_once('libs/yelp-oauth.php');

$CONSUMER_KEY = 'WU26ezEce9xX5gxNVgx5xQ';
$CONSUMER_SECRET = 'iTqGXSSScnmRfyVOEMM2ayqKBCc';
$TOKEN = '3TetnGiCEgXPWEqGjd1myDczFbfLId1D';
$TOKEN_SECRET = 'Wwj6jUXJp4urDozSssg7uCNjfJY';


$API_HOST = 'api.yelp.com';
//$BOUNDS = "$swlat,$swlng|$nelat,$nelng";
$DEFAULT_TERM = "gluten";
//$DEFAULT_TERM = $v;
//$DEFAULT_LOCATION = $_GET['location'];
//$CLL = $ll;
$LL = $ll;
//$DEFAULT_CATEGORY = $_GET['category'];
//$DEFAULT_OFFSET = $_GET['offset'];
//$SEARCH_LIMIT = 3;
$RADIUS_FILTER = $radius;
$SEARCH_PATH = '/v2/search/';
$BUSINESS_PATH = '/v2/business/';


/** 
 * Makes a request to the Yelp API and returns the response
 * 
 * @param    $host    The domain host of the API 
 * @param    $path    The path of the APi after the domain
 * @return   The JSON response from the request      
 */
function request($host, $path) {
    $unsigned_url = "http://" . $host . $path;

    // Token object built using the OAuth library
    $token = new OAuthToken($GLOBALS['TOKEN'], $GLOBALS['TOKEN_SECRET']);

    // Consumer object built using the OAuth library
    $consumer = new OAuthConsumer($GLOBALS['CONSUMER_KEY'], $GLOBALS['CONSUMER_SECRET']);

    // Yelp uses HMAC SHA1 encoding
    $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

    
    $oauthrequest = OAuthRequest::from_consumer_and_token(
        $consumer, 
        $token, 
        'GET', 
        $unsigned_url
    );
    
    // Sign the request
    $oauthrequest->sign_request($signature_method, $consumer, $token);

    // Get the signed URL
    $signed_url = $oauthrequest->to_url();
    //echo "$signed_url";
    // Send Yelp API Call
    $ch = curl_init($signed_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    
    return $data;
}

/**
 * Query the Search API by a search term and location 
 * 
 * @param    $term        The search term passed to the API 
 * @param    $location    The search location passed to the API 
 * @return   The JSON response from the request 
 */
function search($term, $ll, $radius_filter) {
    $url_params = array();
    
    $url_params['term'] = $term ?: $GLOBALS['DEFAULT_TERM'];
    //$url_params['bounds'] = $bounds?: $GLOBALS['BOUNDS'];
    //$url_params['location'] = $location?: $GLOBALS['DEFAULT_LOCATION'];
    //$url_params['cll'] = $cll?: $GLOBALS['CLL'];
    $url_params['ll'] = $ll?: $GLOBALS['LL'];
    $url_params['radius_filter'] = $radius_filter?: $GLOBALS['RADIUS_FILTER'];
    //$url_params['limit'] = $GLOBALS['SEARCH_LIMIT'];
    //$url_params['category_filter'] = $GLOBALS['DEFAULT_CATEGORY'];
    //$url_params['offset'] = $GLOBALS['DEFAULT_OFFSET'];
    $search_path = $GLOBALS['SEARCH_PATH'] . "?" . http_build_query($url_params);
    
    return request($GLOBALS['API_HOST'], $search_path);
}

/**
 * Query the Business API by business_id
 * 
 * @param    $business_id    The ID of the business to query
 * @return   The JSON response from the request 
 */
function get_business($business_id) {
    $business_path = $GLOBALS['BUSINESS_PATH'] . $business_id;
    
    return request($GLOBALS['API_HOST'], $business_path);
}

/**
 * Queries the API by the input values from the user 
 * 
 * @param    $term        The search term to query
 * @param    $location    The location of the business to query
 */
function query_api($term, $ll, $radius_filter) {     
    $response = json_decode(search($term, $ll, $radius_filter));
//    $business_id = $response->businesses[0]->id;
    
//    print sprintf(
//        "%d businesses found, querying business info for the top result \"%s\"\n\n",         
//        count($response->businesses),
//        $business_id
//    );
    
//    $response = get_business($business_id);
    
//    print sprintf("Result for business \"%s\" found:\n", $business_id);
//    print "$response\n";
    
    return $response;
}



/**
 * User input is handled here 
 */
$longopts  = array(
    "term::",
    //"bounds::"
    //"location::",
    //"cll::",
    //"category_filter::",
    //"offset::",
    "ll::",
    "radius_filter::"
);
    
$options = getopt("", $longopts);

$term = $options['term'] ?: '';
//$bounds = $options['bounds'] ?: '';
//$location = $options['location'] ?: '';
//$cll= $options['cll'] ?: '';
//$category_filter = $options['category_filter'] ?: '';
//$offset = $options['offset'] ?: '';
//$ll= $options['ll'] ?: '';
//$radius_filter = $options['radius_filter'] ?: '';


$data = array();
$data = query_api($term, $ll, $radius_filter);
$myJson['meta']['yelp'] = $data;  


foreach ($data->businesses as $business) {
        unset($tipText);
        $vKey=vKey($business->name,$business->location->coordinate->latitude,$business->location->coordinate->longitude);
        
        if (strpos($business->snippet_text,'gluten')) {
                $tipText=$business->snippet_text;
            } elseif(in_array('Gluten-Free', array($business->categories[0][0],$business->categories[1][0],$business->categories[2][0]))) {
                $tipText="This place is listed in Gluten free category on Yelp";
            } else {
                $tipText="We've detected reviews of this place about gluten on Yelp";
            }
//            
//        $tips[$vKey]= array (
//                            'tipFrom' => "Yelp",
//                            'tipText' => $tipText,
//                            'tipUser' => "",
//                            'tipPic' => "",
//                        );    
            
         if ($listed[$vKey]==false) {  
             
                $pics[$vKey][]= array (
                        'picFrom' => "Yelp",
                        'picUrl' => $business->image_url
                        );
                
                $Venues[$vKey]= array(   
                                    'belongTo' => "Yelp",
                                    'name' => $business->name,
                                    'lat' => $business->location->coordinate->latitude,
                                    'lng' => $business->location->coordinate->longitude,
                                    'distance' => $business->distance,
                                    'address' => $business->location->display_address[0],
                                    'zip' => $business->location->postal_code,
                                    'city' => $business->location->city,
                                    'state' => $business->location->display_address[3],
                                    'country' => $business->location->country_code,
                                    'phone' => $business->display_phone,
                                    'category' => implode(",", $business->categories),
                                    'url' => $business->url,
                                    'photo' => $business->image_url,
                                    'tips' => ""

                );
         $listed[$vKey]=true;
         }
         $tips[$vKey]['Yelp']['meta']['it']['match'] = "Elencato tra i locali gluten free su Yelp";
         $tips[$vKey]['Yelp']['meta']['en']['match'] = "Listed as gluten free venue on Yelp";
         $tips[$vKey]['Yelp']['meta']['url'] = $business->url;
    }


///////////////// END YELP 

///////////////// TRIPADVISOR
$TAKey="AIzaSyAP9qlj7u-9HlyIM9d50qz6kgRm6wgUDNg"; 
$TAurl="http://api.tripadvisor.com/api/partner/2.0/location_mapper/42.344978,-71.119030?key=$TAKey-mapper&category=restaurants&q=gluten%20free%20Inn%201023";
///////////////// END TRIPADVISOR    

///////////////// GPLACES
$gpKey="AIzaSyCyFymWaPSGlJDdeVOHsi0L40U_eO_LkbU";
$gpUrl="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$ll&radius=$radius&types=food&keyword=gluten|glutine&key=$gpKey";
$gpData  = json_decode(file_get_contents($gpUrl));

foreach ($gpData->results as $gplace) {
        
        $vKey = vKey($gplace->name,$gplace->geometry->location->lat,$gplace->geometry->location->lng);
        
        // PULL DOWN VENUE DETAILS FROM GOOGLE
        $gpDetailsUri = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$gplace->place_id."&key=$gpKey";
        $gpDetails  = json_decode(file_get_contents($gpDetailsUri));
        //END VENUE DETAILS
        
        /////////////////////// PER OGNI RECENSIONE
//        foreach ($gpDetails->result->reviews as $tip) {   
//                     $tips[$vKey]['Google']['tips'][]= array (
//                            'tipText' => $tip->text,
//                            'tipUser' => $tip->author_name,
//                            'tipUserpic' => $tip->profile_photo_url,
//                            'tipUrl' => '',
//                            'tipImg' => '' 
//                             );     
//        }         
        //////// FINE PER OGNI RECENSIONE
        
        
        $tips[$vKey]['Google']['meta']['it']['match'] = "Identificato come gluten free su Google";
        $tips[$vKey]['Google']['meta']['en']['match'] = "Listed as gluten free on Google";
        $tips[$vKey]['Google']['meta']['url'] = $gpDetails->result->url;
        $gphotoRef=$gplace->photos[0]->photo_reference;                
        $gphoto="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&maxheight=250&photoreference=$gphotoRef&key=$gpKey";
        //$postal_code_url="https://maps.googleapis.com/maps/api/geocode/json?place_id=".$gplace->place_id."&key=$gpKey";
        //$gpZip  = json_decode(file_get_contents($postal_code_url));
        $gpAddress = explode(",", $gplace->vicinity);
        
        ////////////////////// PER OGNI FOTO
        foreach ($gplace->photos as $pic) {
                if ($picIn[$pic->photo_reference]==false) { //// verifica inserimento recensione    
                    $pics[$vKey][]= array (
                        'picFrom' => "Google Places",
                        'picUrl' => "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&maxheight=250&photoreference=".$pic->photo_reference."&key=$gpKey"
                        );
                    $picIn[$pic->photo_reference]=true;
                }

        } //////////////// FINE PER OGNI FOTO
        
        
        if ($listed[$vKey]==false) {
            $Venues[$vKey]['belongTo']="Google Places";
            $Venues[$vKey]['name']=$gplace->name;
            $Venues[$vKey]['lat']=$gplace->geometry->location->lat;
            $Venues[$vKey]['lng']=$gplace->geometry->location->lng;
            $Venues[$vKey]['distance']=distance($geo[0],$geo[1],$gplace->geometry->location->lat,$gplace->geometry->location->lng);
            $Venues[$vKey]['address']=$gpDetails->result->formatted_address;
            $Venues[$vKey]['zip']="";
            $Venues[$vKey]['city']=trim($gpAddress[2]);
            $Venues[$vKey]['state']="";
            $Venues[$vKey]['country']="";
            $Venues[$vKey]['phone']=$gpDetails->result->international_phone_number;
            $Venues[$vKey]['category']=$gplace->types;
            $Venues[$vKey]['url']=$gpDetails->result->website;
            $Venues[$vKey]['photo']=$gphoto;
            $Venues[$vKey]['gpDetails']=$gpDetails;
//            if ($item->venue->photos->groups[0]->items[0]->prefix) {
//                $Venues[$vKey]['photo']=$item->venue->photos->groups[0]->items[0]->prefix."500x500".$item->venue->photos->groups[0]->items[0]->suffix;
//            } 
        }
        $listed[$vKey]=true;
        if ($gplace->permanently_closed) {$Venues[$vKey]['dead'] = true;}
}

///////////////// END GPLACES      


//SORT VENUES X DISTANCE
// from http://esempi.francescodagostino.it/php/array_multisort
    unset($distance);
    foreach ($Venues as $key=> $venue)
        {
            $distance[$key]  = $venue['distance'];
        }
    // Ordinamento di $Venues 
    array_multisort($distance, SORT_ASC, SORT_NUMERIC, $Venues);


//DISTANCE CALCULATOR
function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);

      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

      $angle = atan2(sqrt($a), $b);
      return $angle * $earthRadius;
}

$myJson['meta']['total'] = count($Venues); 
$myJson['venues']=array();
foreach ($Venues as $key => $value) {
                    if ($Venues[$key]['dead']!= true) {
                    $myJson['venues'][]= array(   
                                    'key' => $key,
                                    'belongTo' => $Venues[$key]['belongTo'],
                                    'gfRate' => gfScore($tips[$key]),
                                    'name' => $Venues[$key]['name'],
                                    'lat' => $Venues[$key]['lat'],
                                    'lng' => $Venues[$key]['lng'],
                                    'distance' => intval($Venues[$key]['distance']),
                                    'address' => $Venues[$key]['address'],
                                    'zip' => $Venues[$key]['zip'],
                                    'city' => $Venues[$key]['city'],
                                    'state' => $Venues[$key]['state'],
                                    'country' => $Venues[$key]['country'],
                                    'phone' => $Venues[$key]['phone'],
                                    'category' => $Venues[$key]['category'],
                                    'url' => $Venues[$key]['url'],
                                    'photos' => $Venues[$key]['photo'],
                                    'channels' => $tips[$key],
                                    'pics' =>$pics[$key],
                                    'dead' =>$Venues[$key]['dead'],
                                    'gpDetails' =>$Venues[$key]['gpDetails'],
                                );
                    }

}
$myJson['meta']['gpurl'] = $gpUrl;     
$myJson['meta']['gplaces'] = $gpData;  
file_put_contents($cachePath,json_encode($myJson));

header('Access-Control-Allow-Origin: http://kqmfj7y3-mzarkoff.rhcloud.com:8888',false);
header('Content-Type: application/json; charset=UTF8');
echo json_encode($myJson,JSON_PRETTY_PRINT);
//print_r($myJson);

?>
<?php
//FUNCTIONS
function gfScore($results) {
    //$d = array(1=>"Basso",2=>"Medio",3=>"Locale molto noto e frequentato nella community gluten free della zona."); 
    
    $numMatches = count($results);
    $fqRevs = count($results["Foursquare"]);
    
    if ($numMatches == 3) {
        
        $x['score'] = 100;
        $x['it']['rating'] = "Locale noto e ben frequentato tra la comunità gluten free di zona."; 
        $x['en']['rating'] = "A well-known and popular location in the local gluten free community."; 
        
    } elseif ($numMatches == 2) {
        
        $x['score'] = 4;
        $x['it']['rating'] = "Locale con una buona offerta gluten free.";
        $x['en']['rating'] = "Location with a good gluten free offer.";
        
    } elseif ($numMatches == 1 && $fqRevs == 0) {
        
        $x['score'] = 2;
        $x['it']['rating'] = "Locale indicato come gluten free, ma con poche conversazioni al riguardo.";
        $x['en']['rating'] = "Location indicated as gluten free, but with few comments about it.";
        
    } elseif ($numMatches == 1 && ($fqRevs == 1 || $fqRevs ==2 )) {        
        $x['score'] = 1;
        $x['it']['rating'] = "Alcune conversazioni nei social indicano la possibilità di opzioni gluten free in questo locale.";
        $x['en']['rating'] = "Some conversations on social networks indicate the possibility of gluten free options in this location.";
        
    } elseif ($numMatches == 1 && $fqRevs > 3 ) {        
        $x['score'] = 3;
        $x['it']['rating'] = "Locale molto recensito sulle opzioni gluten free.";
        $x['en']['rating'] = "A much reviewed location on gluten free options.";
    }
     $fqRevs = 0;
    return $x;
    
}
function cacheFile($temp) {
    $delChars=array(",");
    $repChars=array("_");
    return str_replace($delChars, $repChars, $temp);
} 

function vKey($nome,$lat,$lng) {
    settype($lat,"float");
    settype($lng,"float");
    $nome = stripCommons($nome);
    $str1 = 'àáâãäâçèéêëìíîïñôòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ';
    $str2 = 'aaaaaaceeeeiiiinoooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY';
    $geoKey = number_format($lat, 2, '', '').number_format($lng, 2, '', '');
    //$temp1 = ereg_replace("/^[^\W_]+$/", "", strtolower($nome));
    $temp1 = preg_replace("'/[^A-Za-z0-9\-]/'", "", strtolower(strtr($nome,$str1,$str2)));
    $temp2 = str_replace(array('&','.',' ','-','+',"'",'–'), '', $temp1);
    return $temp2.$geoKey;
}

function stripCommons($venueName) {
    $search = array("ristorante","pizzeria","trattoria");
    $replace = array("","","");
    return str_ireplace($search, $replace, $venueName);
}

?>