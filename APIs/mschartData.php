<?php

//Richiede le variabili GET 
// - "IDInst" (N id sensori separati da virgole)
// - "comp_scalar" (il tipo componente)
// - "mu_id" (id dell'unità di misura)
//--------
// Restituisce un elenco di valori della componente specificata di N
// sensori con la MU specificata.
//--------
//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

$IDInst = $_GET['IDInst'];
$comp_ordinal = $_GET['comp_ordinal'];
$mu_id = $_GET['mu_id'];
$csvRow = "";

include_once($_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php');

//Unix_Timestamp(data_public.ReadingTime) AS tstamp,
//DATE_FORMAT(data_public.ReadingTime, \"%Y-%m-%d\") as tstamp,

$sql = "SELECT
  data_public.IDInst,
  v_instrument_global_info.inst,
  v_instrument_global_info.comp_ordinal,
  DATE_FORMAT(data_public.ReadingTime, \"%Y-%m-%d %H:%i\") AS ReadingTime,
  data_public.Value,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name
FROM
  data_public
  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
WHERE
  data_public.IDInst IN ($IDInst) AND
  v_instrument_global_info.comp_ordinal = $comp_ordinal
ORDER BY
  data_public.ReadingTime";

//$rData['meta']['sql'] = $sqlRadar;

$conn = mysql_query($sql, $dbConn) or die("$sqlCheck: " . mysql_error());

//scorro il recordset per popolare i primi dati e l'array degli strumenti presenti
while ($row = mysql_fetch_assoc($conn)) {
    $out["lstinst"][$row["inst"]] = true;
    $out["data"][$row["ReadingTime"]][$row["inst"]] = $row["Value"];
}

//per ogni timestamp
foreach ($out["data"] as $timestamp => $letture) {

    //per ogni serie
    foreach ($out["lstinst"] as $inst => $bool) {
        if (!array_key_exists($inst, $letture)) {
            $out["data"][$timestamp][$inst] = "";
        }
    }
}


$out["lstInst"] = $lstInst;

//############ JSON OUTPUT ######################
if ($_GET['out'] == 'json') {
    header('Content-Type: application/json');
    echo json_encode($out, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

//############ CSV OUTPUT ######################
} elseif ($_GET['out'] == 'csv') {

    //invia header
    header('Content-Type: text/plain');

    //stampa intestazione
    $csvRow = "Data,";
    foreach ($out["lstinst"] as $instName => $value) {
        $csvRow .= "$instName,";
    }
    echo substr($csvRow, 0, -1) . "\n";
    $csvRow = "";
    //per ogni timestamp
    $i = 0;
    $max = count($out["data"]);
    foreach ($out["data"] as $timestamp => $letture) {
        $i = $i + 1;
        $csvRow = "$timestamp,";
        foreach ($out["lstinst"] as $instName => $value) {
            $csvRow .= $letture[$instName] . ",";
        }
        echo substr($csvRow, 0, -1);
        if ($i <> $max) {
            echo "\n";
        }
        $csvRow = "";
    }
}
?>