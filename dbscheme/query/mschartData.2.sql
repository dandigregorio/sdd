﻿SELECT
  data_public.IDInst,
  v_instrument_global_info.inst,
  v_instrument_global_info.comp_scalar,
  data_public.ReadingTime,
  data_public.Value,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name
FROM
  data_public
  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
WHERE
  (data_public.IDInst = 452 OR
    data_public.IDInst = 453) AND
  v_instrument_global_info.comp_scalar = 'Est' AND
  v_instrument_global_info.mu_id = 2
