﻿SELECT
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu,
  v_instrument_global_info.comp_scalar
FROM
  data_public
  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
WHERE
  (data_public.IDInst = 452 OR
    data_public.IDInst = 21427 OR
    data_public.IDInst = 5775)
GROUP BY
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu,
  v_instrument_global_info.comp_scalar
