﻿SELECT
  data_public.IDInst,
  data_public.IDComp,
  data_public.ReadingTime,
  data_public.Value,
  v_instrument_global_info.comp_ordinal,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu,
  v_instrument_global_info.has_data
FROM
  data_public
  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
WHERE
  data_public.IDInst IN (452, 453, 454) AND
  v_instrument_global_info.has_data > 0
