﻿SELECT
  Unix_Timestamp(data_public.ReadingTime) AS tstamp,
  data_public.IDComp,
  data_public.Value AS value
FROM
  data_public,
  v_instrument_publication_status
WHERE
  data_public.IDInst = 10469
