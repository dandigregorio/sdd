﻿SELECT
  v_instrument_global_info.id_scalar,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu
FROM
  v_instrument_global_info
WHERE
  v_instrument_global_info.inst_id IN (452, 4165)
GROUP BY
  v_instrument_global_info.id_scalar,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu
