﻿SELECT
  v_instrument_global_info.inst_id,
  v_instrument_global_info.inst,
  Count(data_public.ID) AS `values`
FROM
  v_instrument_global_info
  LEFT JOIN data_public ON data_public.IDInst = v_instrument_global_info.inst_id
WHERE
  v_instrument_global_info.inst_type_id = 4
GROUP BY
  v_instrument_global_info.inst_id,
  v_instrument_global_info.inst
