﻿SELECT
  v_instrument_global_info.inst_id,
  v_instrument_global_info.inst,
  v_instrument_global_info.has_data AS `values`,
  v_instrument_global_info.status,
  v_instrument_global_info.time_last_collect AS edited,
  v_instrument_global_info.time_last_public AS published,
  v_instrument_global_info.c2p_delta_days AS delta,
  v_instrument_global_info.blocco AS lockstatus,
  v_instrument_global_info.auto AS auto
FROM
  v_instrument_global_info
WHERE
  SUBSTR(v_instrument_global_info.inst, 1, 5) IN ("000L3") AND
  SUBSTR(v_instrument_global_info.inst, 6, 2) IN ("MP") AND
  SUBSTR(v_instrument_global_info.inst, 8, 2) IN ("01") AND
  SUBSTR(v_instrument_global_info.inst, 10, 1) IN ("A")
GROUP BY
  v_instrument_global_info.inst_id,
  v_instrument_global_info.status,
  v_instrument_global_info.time_last_collect,
  v_instrument_global_info.time_last_public,
  v_instrument_global_info.c2p_delta_days,
  v_instrument_global_info.blocco,
  v_instrument_global_info.auto
