﻿SELECT
  data_public.ID,
  data_public.IDInst,
  v_instrument_global_info.inst,
  v_instrument_global_info.id_scalar,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.comp_ordinal,
  Date_Format(data_public.ReadingTime, "%Y-%m-%d %H:%i") AS ReadingTime,
  data_public.Value,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name
FROM
  data_public
  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
WHERE
  data_public.IDInst IN (453, 454) AND
  v_instrument_global_info.id_scalar = 1
ORDER BY
  data_public.ReadingTime
