﻿SELECT
  Date_Format(data_public.ReadingTime, "%Y-%m-%d %H:%i") AS ReadingTime,
  data_public.Value
FROM
  instrument_scalar
  INNER JOIN data_public ON instrument_scalar.IDInst = data_public.IDInst AND instrument_scalar.Component =
    data_public.IDComp
WHERE
  instrument_scalar.IDInst = 456 AND
  instrument_scalar.IDScalar = 2
ORDER BY
  data_public.ReadingTime
