<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php';
header('Content-Type: application/json');

$out = 0;
$idrole = $_GET["idrole"];
$controller = $_GET["controller"];
$action = $_GET["action"];

$url = file_get_contents("../../backend/data/app.json");
$conf = json_decode($url, true);

if ($controller == "" && $action == "") {
    $out = 1;
} elseif ($conf['privs'][intval($idrole)][$controller]["overall"]) {
    $out = 1;
} elseif ($conf['privs'][intval($idrole)][$controller][$action]) {
    $out = $conf['privs'][intval($idrole)][$controller][$action];
}

echo json_encode($out, JSON_NUMERIC_CHECK);
?>