<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php'; 
header('Content-Type: application/json');

$out=array();
$email = $_GET["email"];
$password = $_GET["password"];

$apiCall="http://".$_SERVER['HTTP_HOST']."/API/users/search?email=$email&password=$password";
$json = file_get_contents($apiCall);

if ($json == false) {
    echo json_encode();
    exit;
    }

$user = json_decode($json, true);

session_start();
$myuser = new MyUser(true, $user['email'], $user['role']);
$_SESSION['loggedin'] = serialize($myuser);
$_SESSION['user'] = $user['email'];
$_SESSION['idrole'] = $user['idrole'];
$_SESSION['role'] = $user['role'];
$_SESSION['fname'] = $user['fname'];
$_SESSION['sname'] = $user['sname'];
echo json_encode($_SESSION, JSON_NUMERIC_CHECK | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
?>