<?php
header('Content-Type: application/json');

$out=array();
$email  = $_GET["email"];
$password = $_GET["password"];

$string = file_get_contents("../../backend/data/app.json");
$json_a = json_decode($string, true);

foreach ($json_a['users'] as $index => $user) {
        if ($user['email'] == $email && $user['password'] == $password) {         
            $user['role'] = $json_a['roles'][$user[idrole]];
           echo json_encode($user, JSON_NUMERIC_CHECK | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
           exit;
    }
    
}

echo json_encode();
?>