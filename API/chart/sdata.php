<?php

//Richiede la variabile "matricola" con metodo GET
//--------
//Restituisce ruolo (0,1,2,3,4,5) e label ruolo (District Manager,Branch Manager, ecc)
//in formato Json con chiavi [role] e [label]       
//--------
//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

$sqlFrom = "";
$IDInst = $_GET['IDStrumento'];
$IDScalar = $_GET['IDScalar'];
if ($_GET['from']) {$sqlFrom = "AND data_public.ReadingTime > '".$_GET['from']."' ";}

include_once($_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php');


//$sqlRadar = "SELECT
//Unix_Timestamp(data_public.ReadingTime) AS tstamp,
//DATE_FORMAT(data_public.ReadingTime, \"%Y-%m-%d %H:%i\") AS ReadingTime,
//data_public.Value AS value
//FROM
//  data_public
//WHERE
//  data_public.IDInst = $IDInst AND
//  data_public.id_scalar = $IDScalar";


$sqlRadar = "SELECT
  Date_Format(data_public.ReadingTime, \"%Y-%m-%d %H:%i\") AS ReadingTime,
  data_public.Value
FROM
  instrument_scalar
  INNER JOIN data_public ON instrument_scalar.IDInst = data_public.IDInst AND instrument_scalar.Component =
    data_public.IDComp
WHERE
  instrument_scalar.IDInst = $IDInst AND
  instrument_scalar.IDScalar = $IDScalar
ORDER BY
  data_public.ReadingTime";

//echo $sqlRadar;


$rData['meta']['sql'] = $sqlRadar;

$connRadar = mysql_query($sqlRadar, $dbConn) or die("$sqlCheck: " . mysql_error());

//$rData['data']= $row_radar;

if ($_GET['out'] == 'json') {
    header('Content-Type: application/json');
    while ($row_radar = mysql_fetch_assoc($connRadar)) {
        //print_r($row_radar);
        $datetime = $row_radar["ReadingTime"];
        $value = $row_radar["Value"];
        $out[] = array($datetime=> $value);
    }
    echo json_encode($out, JSON_NUMERIC_CHECK |JSON_PRETTY_PRINT)
            ;
} elseif ($_GET['out'] == 'csv') {
    header('Content-Type: text/plain');
    while ($row_radar = mysql_fetch_assoc($connRadar)) {
        $datetime = $row_radar["ReadingTime"];
        $value = $row_radar["Value"];
       echo "$datetime,$value\n";
    }
    
} 
?>