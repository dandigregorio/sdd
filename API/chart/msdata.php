<?php

//Richiede le variabili GET 
// - "IDInst" (N id sensori separati da virgole)
// - "comp_scalar" (il tipo componente)
// - "mu_id" (id dell'unità di misura)
//--------
// Restituisce un elenco di valori della componente specificata di N
// sensori con la MU specificata.
//--------
//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);
$sqlFrom = "";
$IDInst = $_GET['IDInst'];
$id_scalar = $_GET['id_scalar'];
$mu_id = $_GET['mu_id'];
$csvRow = "";
if ($_GET['from']) {
    $sqlFrom = "AND data_public.ReadingTime > '" . $_GET['from'] . "' ";
}

include_once($_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php');

//Unix_Timestamp(data_public.ReadingTime) AS tstamp,
//DATE_FORMAT(data_public.ReadingTime, \"%Y-%m-%d\") as tstamp,
//$sql = "SELECT
//  data_public.IDInst,
//  v_instrument_global_info.inst,
//  v_instrument_global_info.id_scalar,
//  v_instrument_global_info.comp_scalar,
//  v_instrument_global_info.comp_ordinal,
//  Date_Format(data_public.ReadingTime, \"%Y-%m-%d %H:%i\") AS ReadingTime,
//  data_public.Value,
//  v_instrument_global_info.mu_id,
//  v_instrument_global_info.mu_name
//FROM
//  data_public
//  INNER JOIN v_instrument_global_info ON v_instrument_global_info.inst_id = data_public.IDInst
//WHERE
//  data_public.IDInst IN ($IDInst) AND
//  v_instrument_global_info.id_scalar = $id_scalar ";
//
//if ($sqlFrom) {$sql .= $sqlFrom; }
//      
//$sql .= "ORDER BY
//  data_public.ReadingTime";


$sql = "SELECT
  data_public.ID as id,
  Date_Format(data_public.ReadingTime, \"%Y-%m-%d %H:%i\") AS ReadingTime,
  data_public.Value,
  v_instrument_global_info.inst
FROM 
  instrument_scalar  
 INNER JOIN data_public ON instrument_scalar.IDInst = data_public.IDInst AND instrument_scalar.Component = data_public.IDComp
 LEFT JOIN v_instrument_global_info ON data_public.IDInst = v_instrument_global_info.inst_id  
WHERE
  instrument_scalar.IDInst IN ($IDInst) AND
  instrument_scalar.IDScalar = $id_scalar ";

if ($sqlFrom) {
    $sql .= $sqlFrom;
}

$sql .= " ORDER BY
  data_public.ReadingTime";


$out['meta']['sql'] = $sql;

$conn = mysql_query($sql, $dbConn) or die("$sqlCheck: " . mysql_error());

//scorro il recordset per popolare i primi dati e l'array degli strumenti presenti
$nSerie = array();
while ($row = mysql_fetch_assoc($conn)) {
    if (!$out["lstinst"][$row["inst"]]) {
        $out["lstinst"][$row["inst"]] = true;
        $nSerie[] = $row["inst"];
    };
    $out["data"][$row["ReadingTime"]][$row["inst"]]['val'] = $row["Value"];
    $out["data"][$row["ReadingTime"]][$row["inst"]]['id'] = $row["id"];
}

//per ogni timestamp
foreach ($out["data"] as $timestamp => $letture) {

    //per ogni serie
    foreach ($out["lstinst"] as $inst => $bool) {
        if (!array_key_exists($inst, $letture)) {
            $out["data"][$timestamp][$inst] = "";
        }
    }
}


//############ JSON OUTPUT ######################
if ($_GET['out'] == 'json') {
    //riordino le colonne per serie per alimentare correttamente i grafici
    foreach ($out["data"] as $timestamp => $letture) {
        //per ogni serie
        $myValues = array();
        //riordino i valori delle serie
        foreach ($out["lstinst"] as $label => $bool) {$myValues[$label] = $letture[$label];}
        //li reinserisco nella nuova matrice
        $temp[$timestamp] = $myValues;
    }

    $out["data"] = $temp;
    $temp="";
    header('Content-Type: application/json');
    echo json_encode($out, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

//############ CSV OUTPUT ######################
} elseif ($_GET['out'] == 'csv') {

    //invia header
    header('Content-Type: text/plain');

    //stampa intestazione
    $csvRow = "Data,";
    foreach ($out["lstinst"] as $instName => $value) {
        $csvRow .= "$instName,";
    }
    echo substr($csvRow, 0, -1) . "\n";
    $csvRow = "";
    //per ogni timestamp
    $i = 0;
    $max = count($out["data"]);
    foreach ($out["data"] as $timestamp => $letture) {
        $i = $i + 1;
        $csvRow = "$timestamp,";
        foreach ($out["lstinst"] as $instName => $value) {
            $csvRow .= $letture[$instName]['val'] . ",";
        }
        echo substr($csvRow, 0, -1);
        if ($i <> $max) {
            echo "\n";
        }
        $csvRow = "";
    }
}
?>