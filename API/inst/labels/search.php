<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php');
//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

$rownum = 0;
$out['data']=array();

//ELIMINA LA SESSIONE GET INVIATA DA JS
 unset($_GET["_"]);
 

foreach ($_GET as $key => $value) {
    if ($key=="_") {unset($_GET[$key]);}
    if ($value=="") {unset($_GET[$key]);}
}
 
 
$sql = "SELECT
  v_instrument_global_info.inst_id,
  v_instrument_global_info.inst,
  v_instrument_global_info.has_data AS `values`,
  v_instrument_global_info.status,
  v_instrument_global_info.time_last_collect AS edited,
  v_instrument_global_info.time_last_public AS published,
  v_instrument_global_info.c2p_delta_days AS delta,
  v_instrument_global_info.blocco AS lockstatus,
  v_instrument_global_info.auto AS auto
FROM
  v_instrument_global_info
  ";

if (count($_GET)) {
    $matchSql = "";
    $sql .= "WHERE\n";
    foreach ($_GET as $substr => $matchs) {
        if ($matchs) {
            $sql .= "SUBSTR(v_instrument_global_info.inst, " . sqlizeRange($substr) . ") IN (" . sqlizeValues($matchs) . ") AND \n";
        }
    };
    $sql = substr($sql, 0, -5) . "\n";
}

$sql .= "GROUP BY
  v_instrument_global_info.inst_id,
  v_instrument_global_info.status,
  v_instrument_global_info.time_last_collect,
  v_instrument_global_info.time_last_public,
  v_instrument_global_info.c2p_delta_days,
  v_instrument_global_info.blocco,
  v_instrument_global_info.auto";
//print "<pre>$sql</pre>";

//$out['meta']['sql']=$sql;


$conn = mysql_query($sql, $dbConn) or die("$sqlCheck: " . mysql_error());
//
//$out['data'] = array();
while ($row = mysql_fetch_assoc($conn)) {
    $rownum++;
    $out['data'][] = array(
        'DT_RowId' => $rownum,
        "inst_id" => $row["inst_id"],
        "inst" => $row["inst"],
        "status" => $row["status"],
        "edited" => $row["edited"],
        "published" => $row["published"],
        "delta" => intval($row["delta"]),
        "lockstatus" => $row["lockstatus"],
        "auto" => intval($row["auto"]),
        "valCounts" => intval($row["values"]),
        "set" => ""
    );
}
header('Content-Type: application/json');
echo json_encode($out, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);


############ FUNCTIONS ######################

function sqlizeValues($params) {
    $str = "";
    $param = explode(",", $params);
    foreach ($param as $val) {
        $str .= "'" . $val . "',";
    }
    return substr($str, 0, -1);
}

function sqlizeRange($params) {
    $delimit = explode("-", $params);
    return ($delimit[0] . ", " . $delimit[1]);
}

?>