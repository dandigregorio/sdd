<?php

//Richiede la variabile "inst_id" con metodo GET
//--------
// Restituisce intero recordset da v_instrument_global_info dell'ID chiamato
// (inst_id,inst,inst_type_id, comp_ordinal, comp_scalar, mu_id, mu_name, mu)
// NB. N record = N componenti       
//--------
//ini_set('display_errors','On'); 
//ini_set('error_reporting','E_ALL | E_STRICT'); 
//error_reporting(E_ALL);

include_once($_SERVER['DOCUMENT_ROOT'] . '/config/bootstrap.php');
$ids = explode(",", $_GET['IDStrumenti']);
$where = "";
if (count($ids) > 0) {$where = " WHERE v_instrument_global_info.inst_id IN (".$_GET['IDStrumenti'].") ";}


$sql = "SELECT
v_instrument_global_info.id_scalar,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu
FROM
  v_instrument_global_info
$where
GROUP BY
  v_instrument_global_info.id_scalar,
  v_instrument_global_info.comp_scalar,
  v_instrument_global_info.mu_id,
  v_instrument_global_info.mu_name,
  v_instrument_global_info.mu";

//$rData['meta']['sql'] = $sqlRadar;

$conn = mysql_query($sql, $dbConn) or die("$sqlCheck: " . mysql_error());

while ($row = mysql_fetch_assoc($conn)) {
    $out[] = array(
        "id_scalar" => $row['id_scalar'],
        "comp_scalar" => $row['comp_scalar'],
        "mu_id" => $row['mu_id'],
        "mu_name" => $row['mu_name'],
        "mu" => $row['mu']);
}
//$rData['data']= $row_radar;




header('Content-Type: application/json');
echo json_encode($out, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
?>