<?php
////////////////////////////////////////////////////
///////////  CLASSES ///////////////////////////////
////////////////////////////////////////////////////
// ssh://561e6be67628e1883c0000fc@v10-gmama.rhcloud.com/~/git/v10.git/

class appController {

    const
            appFolder = "",
            debug = true,
            enc_key = 'pmt105',
            appTheme = "adminLTE", // The name of the active theme
            appUrl = "http://192.168.2.144:8080",
////            DEV SETTINGS  
              appPath="/",              
              dbName="db_sad",
              dbHost="192.168.2.3:5477",
              dbUser="testuser",
              dbPsw="testpassword";  
//            PROD SETTINGS        
//              appPath="/var/lib/openshift/561e6be67628e1883c0000fc/app-root/runtime/repo/",
//              dbName="gmama",
//              dbHost="127.10.56.2:3306",
//              dbUser="adminu6v7C7Y",
//              dbPsw="_kZYJ-MaGFcn";      

    public function hrefLangUri($lang) {
        if ($lang == 'en') {
            $host = "http://gastromama.com";
        } else if ($lang == 'it') {
            $host = "http://gastromama.it";
        } else {
            return false;
        }
        return $host . $_SERVER['REQUEST_URI'];
    }

    public function notFound() {
    }

    public function notLogged() {
        if ($_SESSION['user']) {
            return false;
        } else {
            return true;
        }
    }

    public function notAuth() {
        //lang=en&page=venue&key=$1
        $auth = file_get_contents("http://" . $_SERVER['HTTP_HOST'] . "/API/users/auth?idrole=" . $_SESSION['idrole'] . "&controller=" . $_GET['page'] . "&action=" . $_GET['action']);
        $_SESSION["authurl"] = "http://" . $_SERVER['HTTP_HOST'] . "/API/users/auth?idrole=" . $_SESSION['idrole'] . "&controller=" . $_GET['page'] . "&action=" . $_GET['action'];
        $_SESSION["auth"] = $auth;
        if ($auth == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function metaRobots() {
        return "NOINDEX, NOFOLLOW";
    }

    public function getPage() {

        $qPage = "home";
        $qAction = "default";

        if ($_GET['page']) {
            $qPage = $_GET['page'];
        }
        if ($_GET['action']) {
            $qAction = $_GET['action'];
        }

        if ($this->notFound()) {
            $qPage = "404";
            $qAction = "default";
        }

        return "$qPage/$qAction";
    }

    public function linkPage($page, $action, $queryValues) {
        // USAGE: $myApp->linkPage(<CONTROLLER>,<ACTION>,<ARRAY WITH GET KEYS/VALUES>);
        $tail = "";
        $query = "";

        $tail = $this->linkHome($this->getLang());
        if ($page) {
            $tail .= "page/$page/";
        }
        if ($action) {
            $tail .= '/' . $action;
        }

        if (count($queryValues)) {
            $query .= "?";
            while (list($key, $value) = each($queryValues)) {
                $query .= $key . "=" . urlencode($value);
                $query .= "&";
            }
            $tail .= $query;
        }

        return $tail;
    }

    public function getLang() {
        if ($_GET['lang'] && $_GET['lang'] != 'undefined') {
            return $_GET['lang'];
        } else {
            return 'en';
        }
    }

    public function getTitle() {
        
    }

    public function linkHome($l) {

        if ($l == 'pt') {
            $myDir = 'pt/';
        } else if ($l == 'en' || $l == 'it') {
            $myDir = '';
        }
        $linkH = "http://" . $_SERVER['HTTP_HOST'] . "/" . $myDir;
        return $linkH;
    }

    public function printHeader() {
        if ($this->notFound()) {
            header("HTTP/1.0 404 Not Found");
            echo "Error 404 - Not found ";
            echo print_r($_SESSION);
            exit;
        }
        if ($this->notLogged()) {
            header("location:" . self::appUrl . "/login.php");
            exit;
        }
        if ($this->notAuth()) {
            header("HTTP/1.0 401 Unauthorized");
            echo "Error 401 - Unauthorized";
            exit;
        }
        require self::appPath . 'pages/' . $this->getPage() . '/controller.php';
        include_once self::appPath . 'templates/' . self::appTheme . '/header.php';
    }

    public function printFooter() {
        include_once self::appPath . 'templates/' . self::appTheme . '/libs.php';
        include_once self::appPath . 'pages/' . $this->getPage() . '/libs.php';

        echo("<script>\n");
        echo("     delete themeTXT;\n");
        echo("     var themeTXT = new Object();\n");
        echo("     var pageTXT = new Object();\n");
        echo("     var varGet= new Array();\n");
        echo("     var varSess= new Array();\n");
        if (!$_GET['lang']) {
            echo("     var varGet[\"lang\"] = 'en';\n");
        }


        foreach ($_GET AS $key => $value) {
            echo("     varGet[\"$key\"] = '$value';\n");
        }

        echo ("     varSess[\"user_id\"]='" . unserialize($_SESSION['loggedin'])->id . "';\n");
        echo ("     varSess[\"idrole\"]='" . $_SESSION['idrole']. "';\n");
        echo ("     varSess[\"role\"]='" . $_SESSION['role']. "';\n");
        $themeLoc = '/templates/' . self::appTheme . '/lang/' . $this->getLang() . '.json';
        $pageLoc = '/pages/' . $this->getPage() . '/lang/' . $this->getLang() . '.json';
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $themeLoc)) {
            echo ('    $.getJSON( "' . $themeLoc . '", function( data ) {themeTXT = data; console.log(themeTXT);})' . "\n");
        }
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $pageLoc))
            echo ('    $.getJSON( "' . $pageLoc . '", function( data ) {pageTXT = data; console.log(pageTXT);})' . "\n");
        echo("</script>");
        echo '<script src="/templates/' . self::appTheme . '/js/script.js' . '"></script>';
        echo '<script src="/pages/' . $this->getPage() . '/script.js' . '"></script>';
        include_once self::appPath . 'templates/' . self::appTheme . '/footer.php';
    }

    public function callController() {
        include_once self::appPath . 'controllers/' . $_POST['controller'] . '.php';
        include_once self::appPath . 'controllers/php2jsVars.php';
        echo '<script src="' . self::appPath . 'controllers/' . $this->getPage() . '/script.js' . '"></script>';
        include_once self::appPath . 'templates/' . self::appTheme . '/footer.php';
    }

}

class MyUser {

    public $logged = false;
    public $username = 'none';
    public $role = 'nil';
    public $avatar = "";
    public $meta = "";
    public $id = "";

    public function __construct($a, $b, $c) {
        if ($b <> "") {
            $cryptB = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(appController::enc_key), $b, MCRYPT_MODE_CBC, md5(md5(appController::enc_key))));
        } else {
            $cryptB = "";
        }
        if ($c <> "") {
            $cryptC = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(appController::enc_key), $c, MCRYPT_MODE_CBC, md5(md5(appController::enc_key))));
        } else {
            $cryptC == "";
        }
        $this->logged = $a;
        $this->username = $cryptB;
        $this->role = $cryptC;
        $usrData = json_decode(file_get_contents("http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . "/backend/getUserInfo.php?key=" . urlencode($cryptB) . "&out=json"));
        $this->avatar = $usrData->data->avatar;
        $this->id = $usrData->data->id;
    }

}

class MyMailer {

    //FOR MORE INFORMATIONS AND OPTIONS 
    //http://azure.microsoft.com/en-us/documentation/articles/store-sendgrid-php-how-to-send-email/

    const sendgridUser = "gmama",
            sendgridPsw = "kaos6969";

    public function __construct($from, $to) {

        include appController::appPath . 'libs/swiftmailer/lib/swift_required.php';
        $this->from = $from;
        $this->to = $to;
    }

    public function sendEmail($subject, $text, $html) {
        // Setup Swift mailer parameters
        $transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 587);
        $transport->setUsername(self::sendgridUser);
        $transport->setPassword(self::sendgridPsw);
        $swift = Swift_Mailer::newInstance($transport);

        // Create a message (subject)
        $message = new Swift_Message($subject);

        // attach sender and recipient
        $message->setFrom($this->from);
        $message->setTo($this->to);

        // attach the body of the email           
        if ($html) {
            $message->setBody($html, 'text/html');
            if ($text) {
                $message->addPart($text, 'text/plain');
            }
        } else {
            $message->setBody($text, 'text/plain');
        }

        //$message->attach(Swift_Attachment::fromPath("path\to\file")->setFileName("file_name"));
        // send message 
        if ($recipients = $swift->send($message, $failures)) {
            return $recipients;
        }
        // something went wrong =(
        else {
            //return($failures);
            return '0';
        }
    }

}

////////////////////////////////////////////////////
///////////  FUNCTIONS    //////////////////////////
////////////////////////////////////////////////////

function dec($encrypt_key) {
    if ($encrypt_key) {
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(appController::enc_key), base64_decode($encrypt_key), MCRYPT_MODE_CBC, md5(md5(appController::enc_key))), "\0");
    } else {
        return "";
    }
}

?>